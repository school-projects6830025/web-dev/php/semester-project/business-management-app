<!DOCTYPE html>
<html lang="en">
<head>
    <title><?php echo (!empty($pageTitle) ? $pageTitle . ' - ' : '') ?>Business Manager</title>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css"
          integrity="sha384-zCbKRCUGaJDkqS1kPbPd7TveP5iyJE0EjAuZQTgFLD2ylzuqKfdKlfG/eSrtxUkn" crossorigin="anonymous">
    <link href="./external/jquery-ui-1.13.2/jquery-ui.min.css" rel="stylesheet">
    <link href="./external/jquery-ui-1.13.2/jquery-ui.theme.min.css" rel="stylesheet">
    <link href="./external/jquery-ui-1.13.2/jquery-ui.structure.min.css" rel="stylesheet">

    <script src="./external/jquery-3.7.1.min.js"></script>
    <script src="./external/jquery-ui-1.13.2/jquery-ui.min.js"></script>
</head>
<body>
<header class="container bg-dark p-2">
    <div class="row no-gutters justify-content-between">
        <h1 class="text-white p-2">Business Manager <?php echo (!empty($pageTitle) ? ' - ' . $pageTitle : '') ?></h1>
        <?php

        if ($pageTitle == "Login") {
            echo '</div>';
            echo '</header>
                        <body>
                        <main class="container p-4">';
            return;
        }

        if (empty($_SESSION) || empty($pageTitle)) {
            header('Location: logout.php');
            exit();
        }

        echo '<div class="d-flex flex-column">
            <a href="logout.php" class="btn btn-danger my-1" style="margin: 0 2px">Sign out</a>
            <span class="badge badge-warning">' . $_SESSION['name'] . '</span>
            <span class="badge badge-info">' . $_SESSION['permission'] . '</span>
        </div>
    </div>
</header>';

if ($pageTitle == "No permission") {
    echo '<main class="container px-4">';
    return;
}

?>