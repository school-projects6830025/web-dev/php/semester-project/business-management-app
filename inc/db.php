<?php
/** @var \PDO $db - connection to database */
$db = new PDO('mysql:host=127.0.0.1;dbname=name;charset=utf8', 'username', 'password');

$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);