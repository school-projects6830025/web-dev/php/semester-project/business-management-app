<?php
if (empty($_SESSION) || empty($pageTitle)) {
    header('Location: logout.php');
    die();
}

echo '<nav class="container bg-dark p-2">
        <div class="row no-gutters justify-content-evenly">';
if ($_SESSION['permission'] == 'secretary') {
    if ($pageTitle == "Dashboard") {
        echo '<a href="index.php" class="pages btn btn-light my-1">Dashboard</a>
          <a href="accounts.php" class="pages btn btn-light my-1">Accounts</a>
          <a href="services.php" class="pages btn btn-light my-1">Services</a>
          <a href="edit.php?action=add" class="btn btn-primary my-1">Add new Post</a>';
    }
} else if ($_SESSION['permission'] == 'admin') {
    if ($pageTitle == "Dashboard" || $pageTitle == "Finance" || $pageTitle == "Scheduler" || $pageTitle == 'Commission') {
        echo '<a href="index.php" class="pages btn btn-light my-1">Dashboard</a>
              <a href="scheduler.php" class="pages btn btn-light my-1">Scheduler</a>
              <a href="commission.php" class="pages btn btn-light my-1">Commission</a>
              <a href="finance.php" class="pages btn btn-light my-1">Finance</a>
              <a href="accounts.php" class="pages btn btn-light my-1">Accounts</a>
              <a href="services.php" class="pages btn btn-light my-1">Services</a>
              <a href="reorder.php" class="pages btn btn-light my-1">Reorder</a>
              <a href="edit.php?action=add" class="btn btn-primary my-1">Add new Post</a>';
    }
}
echo '</div>
    </nav>';
?>
<style>
    .pages {
        margin: 0 2px;
    }
</style>

<script>
    let currentPage = window.location.pathname.split('/').pop();
    $('a.pages').each(function() {
        let href = $(this).attr('href').split('/').pop();
        if (href === currentPage) {
            $(this).removeClass('btn-light').addClass('btn-secondary');
        } else {
            $(this).removeClass('btn-secondary').addClass('btn-light');
        }
    });
</script>
