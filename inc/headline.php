<?php
if (empty($_SESSION) || empty($pageTitle)) {
    header('Location: logout.php');
    exit();
}

echo '<main class="container p-0">';
if (isset($_SESSION['operation'])) {
    if ($_SESSION['operation'] == 'success') {
        echo '<div class="alert alert-success border border-success my-2" role="alert">
            '.$_SESSION['announce'].'
                </div>';
    } else if ($_SESSION['operation'] == 'fail') {
        echo '<div class="alert alert-danger border border-danger my-2" role="alert">
                '.$_SESSION['announce'].'
                </div>';
    }
    unset($_SESSION['operation']);
    unset($_SESSION['announce']);
}

echo '<main class="container px-4">';
if (in_array($pageTitle, array("Reorder", "Add new Post", "Edit Post", "Accounts", "Services"))) {
    echo '<h2 class="py-1 px-2">Submit form</h2>';
} else if (in_array($pageTitle, array("Finance", "Dashboard", "Commission", "Scheduler"))) {
    echo '<h2 class="py-1 px-2">Filter</h2>';
}