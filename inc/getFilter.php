<?php
require_once 'inc/db.php';
/** @var \PDO $db */

$sql = "SELECT posts.*, companies.name AS company_name, users.name AS worker_name, services.name AS service_name, u.name AS creator_name
                           FROM posts LEFT JOIN companies ON posts.company_id = companies.company_id LEFT JOIN users ON posts.worker_id = users.user_id
        LEFT JOIN services ON posts.service_id = services.service_id LEFT JOIN users AS u ON posts.user_id = u.user_id WHERE 1";

if (!empty($_GET['company'])) {
    $sql .= " AND (posts.company_id LIKE :companyId OR (:companyId = '%' AND posts.company_id IS NULL))";
}

if ($_SESSION['permission'] == 'worker') {
    $sql .= " AND worker_id LIKE :workerId";
} else if (!empty($_GET['worker'])) {
    $sql .= " AND (posts.worker_id LIKE :workerId OR (:workerId = '%' AND posts.worker_id IS NULL))";
}

if (!empty($_GET['service'])) {
    $sql .= " AND (posts.service_id LIKE :serviceId OR (:serviceId = '%' AND posts.service_id IS NULL))";
}

if (!empty($_GET['datepickerFrom'])) {
    $date = DateTime::createFromFormat('d-m-Y H:i:s', $_GET['datepickerFrom']);
    $newDateFrom = $date->format('Y-m-d H:i:s');
    $sql .= " AND posts.date_created>=:datepickerFrom";
}

if (!empty($_GET['datepickerTo'])) {
    $date = DateTime::createFromFormat('d-m-Y H:i:s', $_GET['datepickerTo']);
    $newDateTo = $date->format('Y-m-d H:i:s');
    $sql .= " AND posts.date_created<=:datepickerTo";
}

if (!empty($_GET['costFrom']) || $_GET['costFrom'] == 0) {
    $sql .= " AND cost >= :costFrom";
}

if (!empty($_GET['costTo']) || $_GET['costTo'] == 0) {
    $sql .= " AND cost <= :costTo";
}

if (!empty($_GET['payment'])) {
    $sql .= " AND payment LIKE :payment";
}

$sql .= " ORDER BY posts.date_created DESC;";
$dataQuery = $db->prepare($sql);

if (!empty($_GET['company'])) {
    $dataQuery->bindValue(':companyId', $_GET['company']);
}

if ($_SESSION['permission'] == 'worker') {
    $dataQuery->bindValue(':workerId', $_SESSION['user_id']);
} else if (!empty($_GET['worker'])) {
    $dataQuery->bindValue(':workerId', $_GET['worker']);
}

if (!empty($_GET['service'])) {
    $dataQuery->bindValue(':serviceId', $_GET['service']);
}

if (!empty($_GET['datepickerFrom'])) {
    $dataQuery->bindValue(':datepickerFrom', $newDateFrom);
}

if (!empty($_GET['datepickerTo'])) {
    $dataQuery->bindValue(':datepickerTo', $newDateTo);
}

if (!empty($_GET['costFrom']) || $_GET['costFrom'] == 0) {
    $dataQuery->bindValue(':costFrom', $_GET['costFrom']);
}

if (!empty($_GET['costTo']) || $_GET['costTo'] == 0) {
    $dataQuery->bindValue(':costTo', $_GET['costTo']);
}

if (!empty($_GET['payment'])) {
    $dataQuery->bindValue(':payment', $_GET['payment']);
}

$dataQuery->execute();
if (!isset($_SESSION['operation']) && !isset($_SESSION['announce'])) {
    $_SESSION['operation'] = 'success';
    $_SESSION['announce'] = 'Posts filtered.';
}