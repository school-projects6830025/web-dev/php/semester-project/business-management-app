<?php
require_once 'db.php';
/** @var \PDO $db */

$query = $db->prepare('SELECT * FROM companies ORDER BY name;');
$query->execute();
$dbCompanies = $query->fetchAll(PDO::FETCH_ASSOC);
echo '<form method="get">
<div class="form-row">
        <div class="form-group col-8 col-sm-5 col-md-4 col-lg-3 col-xl-3" style="min-width: 145px">
                <div class="d-flex flex-column">
                    <label style="margin-bottom: 8px">Company</label>';
if (!empty($dbCompanies)) {
    echo '<div class="btn-group btn-group-toggle" data-toggle="buttons">';
    if (isset($_GET['company'])) {
        echo '<label class="btn btn-secondary ' . (($_GET['company'] == "%") ? 'active' : '') . '" id="companyAll" data-toggle="buttons">
                            <input type="radio" name="company" id="company0" autocomplete="off" value="%" ' . (($_GET['company'] == "%") ? 'checked' : '') . '>All
                        </label>';
        foreach ($dbCompanies as $index => $dbCompany) {
            echo '<label class="btn btn-secondary ' . (($_GET['company'] == $dbCompany['company_id']) ? 'active' : '') . '">
                                    <input type="radio" name="company" id="company' . ++$index . '" autocomplete="off" value="' . $dbCompany['company_id'] . '" ' . (($_GET['company'] == $dbCompany['company_id']) ? 'checked' : '') . '> ' . htmlspecialchars($dbCompany['name'] ?? '') . '
                                </label>';
        }
    } else {
        echo '<label class="btn btn-secondary ' . ((isset($_SESSION['company']) && $_SESSION['company'] == "%") ? 'active' : '') . '" id="companyAll" data-toggle="buttons">
                            <input type="radio" name="company" id="company0" autocomplete="off" value="%" ' . ((isset($_SESSION['company']) && $_SESSION['company'] == "%") ? 'checked' : '') . '>All
                                </label>';
        foreach ($dbCompanies as $index => $dbCompany) {
            echo '<label class="btn btn-secondary ' . ((isset($_SESSION['company']) && $_SESSION['company'] == $dbCompany['company_id']) ? 'active' : '') . '">
                                <input type="radio" name="company" id="company' . ++$index . '" autocomplete="off" value="' . $dbCompany['company_id'] . '" ' . ((isset($_SESSION['company']) && $_SESSION['company'] == $dbCompany['company_id']) ? 'checked' : '') . '> ' . htmlspecialchars($dbCompany['name'] ?? '') . '
                            </label>';
        }
    }
}
echo '</div>
    </div>
    </div>';


$query = $db->prepare('SELECT * FROM users ORDER BY name;');
$query->execute();
$workers = $query->fetchAll(PDO::FETCH_ASSOC);
echo '<div class="form-group col-3 col-sm-3 col-md-3 col-lg-3 col-xl-3" style="min-width: 145px">
        <label for="worker">Employee</label>
        <select class="form-control" id="worker" name="worker">
            <option value="%">All</option>';
foreach ($workers as $worker) {
    echo '<option value="' . $worker['user_id'] . '" ' . ((isset($_GET['worker']) && $_GET['worker'] == $worker['user_id']) ? 'selected' : '') . '>' . htmlspecialchars($worker['name']) . '</option>';
}
echo '</select> </div>';

$query = $db->prepare('SELECT * FROM services ORDER BY name;');
$query->execute();
$services = $query->fetchAll(PDO::FETCH_ASSOC);

echo '<div class="form-group col col-sm col-md col-lg col-xl" style="min-width: 145px">
        <label for="service">Service</label>
            <select class="form-control" id="service" name="service">
                <option value="%">All</option>';

foreach ($services as $service) {
    echo '<option value="' . $service['service_id'] . '" ' . ((isset($_GET['service']) && $_GET['service'] == $service['service_id']) ? 'selected' : '') . '>' . htmlspecialchars($service['name']) . '</option>';
}

echo '</select> </div> </div>';

echo '<div class="form-row no-gutters">
        <div class="form-group col-6 col-sm-4 col-md-3 col-lg-3 col-xl-3" id="costValidationFrom">
                <label for="costFrom">Service Cost from</label>
                <div class="input-group has-validation" id="costs">
                    <div class="input-group-prepend">
                        <span class="input-group-text">€</span>
                    </div>
                    <input type="number" min="-9999" max="9999" maxlength="4" value="' . htmlspecialchars($_GET['costFrom'] ?? '') . '" id="costFrom" name="costFrom" class="form-control" placeholder="Cost From">
                    <div class="invalid-feedback">Cost must be between -9999 and 9999</div>
                </div>
        </div>
        
        <div class="form-group col-6 col-sm-3 col-md-3 col-lg-3 col-xl-3" id="costValidationTo">
                <label for="costTo">Service Cost to</label>
                <div class="input-group has-validation" id="costs">
                    <input type="number" min="-9999" max="9999" value="' . htmlspecialchars($_GET['costTo'] ?? '') . '" id="costTo" name="costTo" class="form-control" placeholder="Cost To">
                    <div class="invalid-feedback">Cost must be between -9999 and 9999</div>
                </div>
        </div>';

?>
        <script>
        $(document).ready(function () {
            $("#costFrom").on("input", function() {
                if (/^-?\d{1,4}$/.test($(this).val()) || !$(this).val()) {
                    $("#costValidationFrom").removeClass('was-validated')
                } else {
                    $("#costValidationFrom").addClass('was-validated')
                }
            });

            $("#costTo").on("input", function() {
                if (/^-?\d{1,4}$/.test($(this).val()) || !$(this).val()) {
                    $("#costValidationTo").removeClass('was-validated')
                } else {
                    $("#costValidationTo").addClass('was-validated')
                }
            });
        });
        </script>
<?php
echo '<div class="form-group col">
            <div class="d-flex flex-column">
                <label style="margin-bottom: 8px">Payment</label>';
echo '<div class="btn-group btn-group-toggle" id="payment" data-toggle="buttons">';
if (isset($_GET['payment'])) {
    echo '<label class="btn btn-secondary ' . (($_GET['payment'] == "%") ? 'active' : '') . '" id="paymentAll">
                            <input type="radio" name="payment" id="payment0" autocomplete="off" value="%" ' . (($_GET['payment'] == "%") ? 'checked' : '') . '>All</label>';
    echo '<label class="btn btn-secondary ' . (($_GET['payment'] == "cash") ? 'active' : '') . '">
                            <input type="radio" name="payment" id="payment1" autocomplete="off" value="cash" ' . (($_GET['payment'] == "cash") ? 'checked' : '') . '>cash</label>';
    echo '<label class="btn btn-secondary ' . (($_GET['payment'] == "card") ? 'active' : '') . '">
                            <input type="radio" name="payment" id="payment2" autocomplete="off" value="card" ' . (($_GET['payment'] == "card") ? 'checked' : '') . '>card</label>';
    echo '<label class="btn btn-secondary ' . (($_GET['payment'] == "voucher") ? 'active' : '') . '">
                            <input type="radio" name="payment" id="payment3" autocomplete="off" value="voucher" ' . (($_GET['payment'] == "voucher") ? 'checked' : '') . '>voucher</label>';

} else {
    echo '<label class="btn btn-secondary active" id="paymentAll">
                            <input type="radio" name="payment" id="payment0" autocomplete="off" value="%" checked>All
                        </label>';
    echo '<label class="btn btn-secondary">
                            <input type="radio" name="payment" id="payment1" autocomplete="off" value="cash">cash</label>';
    echo '<label class="btn btn-secondary">
                            <input type="radio" name="payment" id="payment2" autocomplete="off" value="card">card</label>';
    echo '<label class="btn btn-secondary">
                            <input type="radio" name="payment" id="payment3" autocomplete="off" value="voucher">voucher</label>';
}
echo '</div>
    </div>
    </div>
</div>';

echo '<script>
        $(document).ready(function () {
            $(".btn-group-toggle label").click(function () {
                $(this).siblings().removeClass(\'active\');

                $(this).addClass(\'active\');

                $(this).find(\'input[type="radio"]\').prop(\'checked\', false);

                $(this).find(\'input[type="radio"]\').prop(\'checked\', true);
            });
        });
    </script>';

echo '<div class="form-row">
            <div class="form-group col-6 px-1">
            <label for="datepickerFrom">Date From</label>
            <div class="input-group">';
            echo '<input type="text" onfocus="this.blur()" id="datepickerFrom" name="datepickerFrom" class="form-control" value="' . htmlspecialchars($_GET['datepickerFrom'] ?? '') . '" placeholder="Date from:">';
            //echo '<input type="date" id="datepickerFrom" name="datepickerFrom" class="form-control" value="' . htmlspecialchars($_GET['datepickerTo'] ?? '') . '" placeholder="Date to:"/>';'
            echo '<div class="input-group-append">
                    <button type="button" id="clearButtonFrom" class="btn btn-outline-secondary">&#x2715;</button>
                </div>
            </div>
        </div>
        <div class="form-group col-6 px-1">
            <label for="datepickerTo">Date To</label>
            <div class="input-group">';
            echo '<input type="text" onfocus="this.blur()" id="datepickerTo" name="datepickerTo" class="form-control" value="' . htmlspecialchars($_GET['datepickerTo'] ?? '') . '" placeholder="Date to:">';
            //echo '<input type="date" id="datepickerTo" name="datepickerTo" class="form-control" value="' . htmlspecialchars($_GET['datepickerTo'] ?? '') . '" placeholder="Date to:"/>';
            echo '<div class="input-group-append">
                    <button type="button" id="clearButtonTo" class="btn btn-outline-secondary">&#x2715;</button>
                </div>
            </div>
            </div>
    </div>';

    echo '<button type="reset" class="btn btn-light">Clear</button>
    <button type="submit" class="btn btn-primary">Filter</button>
    </form>';

?>
        <script>
            $(document).ready(function () {

                $('#datepickerFrom').datepicker({ dateFormat: 'dd-mm-yy 00:00:00' });
                $('#datepickerTo').datepicker({ dateFormat: 'dd-mm-yy 23:59:59' });

                $('#clearButtonFrom').click(function() {
                    $('#datepickerFrom').val('');
                });
                $('#clearButtonTo').click(function() {
                    $('#datepickerTo').val('');
                });
                $('.btn[type="reset"]').click(function() {
                    $('#datepickerFrom').val('');
                    $('#datepickerTo').val('');
                });
            });
        </script>
<?php
