<?php
session_start();

require_once 'inc/db.php';
/** @var \PDO $db */

if (empty($_SESSION) || empty($pageTitle)) {
    header('Location: logout.php');
    die();

} else {
    $query = $db->prepare('SELECT * FROM users WHERE users.user_id=:user_id LIMIT 1;');
    $query->execute([
        ':user_id' => $_SESSION['user_id']
    ]);
    $rows = $query->fetchAll(PDO::FETCH_ASSOC);
    if (empty($rows)) {
        header('Location: logout.php');
        die();
    }

    if (!in_array($_SESSION['permission'],array('worker', 'secretary', 'admin')) ) {
        header('Location: logout.php');
        die();
    }

    //Authorisation
    if ( ($_SESSION['permission'] == 'worker' && $pageTitle != "Dashboard")
        || (($_SESSION['permission'] == 'secretary') && in_array($pageTitle, array("Reorder", "Finance", "Commission", "Scheduler"))) ) {
        $pageTitle = "No permission";
        include_once 'inc/header.php';
        echo '<div class="font-weight-bold alert alert-danger border border-danger my-2" role="alert">You don\'t have permission to visit this page! Log out and Log in again!</div>';
        include_once 'inc/footer.php';
        die();
    }

}