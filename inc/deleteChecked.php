<?php
echo '<form method="post">';
echo '<div class="row no-gutters justify-content-between">';
echo '<div class="form-row form-group">';
echo '<button type="button" class="check_all btn btn-dark my-1" name="checkAll">Check All</button>
        <button type="button" class="uncheck_all btn btn-dark my-1" name="uncheckAll">Uncheck All</button>
        </div>';
echo '<div class="form-row form-group">
            <input type="hidden" name="confirmFlag" id="confirmFlag" value="">
            <button class="btn btn-danger my-1" type="submit" onclick="return confirmDelete()" name="deleteChecked" id="deleteChecked">Delete Checked</button>
            </div>
        </div>';
?>
    <script>
        function confirmDelete() {
            let checkedIds = [];
            const checkboxes = document.querySelectorAll('.checkbox:checked');

            checkboxes.forEach(function(checkbox) {
                checkedIds.push(checkbox.value);
            });
            //if (checkedIds.length > 0 && confirm("Delete : " + checkedIds.join(', ') + "?")) {
            if (confirm("Delete " + checkedIds.length + " Posts?")) {
                document.getElementById('confirmFlag').value = 'true';
                return true;
            } else {
                document.getElementById('confirmFlag').value = 'false';
                return false;
            }
        }
    </script>
<?php