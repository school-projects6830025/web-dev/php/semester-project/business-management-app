<?php
$pageTitle = "Commission";
require_once 'inc/userCheck.php';
require_once 'inc/db.php';
/** @var \PDO $db */

if (!empty($_POST)) {
    if (isset($_POST['deleteChecked'])) {
        if (!empty($_POST['ids']) && isset($_POST['confirmFlag'])) {
            if ($_POST['confirmFlag'] === 'true') {
                foreach ($_POST['ids'] as $userId) {
                    $deleteIds = explode(", ", $userId);
                    $placeholders = implode(', ', array_fill(0, count($deleteIds), '?'));
                    $deleteQuery = $db->prepare("DELETE FROM posts WHERE post_id IN ($placeholders)");
                    $deleteQuery->execute($deleteIds);
                }
                $_SESSION['operation'] = 'success';
                $_SESSION['announce'] = 'Filtered Posts of checked employees deleted.';
            }
        } else {
            $_SESSION['operation'] = 'fail';
            $_SESSION['announce'] = 'Nothing checked, nothing to delete.';
        }
    }
}


$berlinTZ = new DateTimeZone("Europe/Berlin");
$curDate = new DateTimeImmutable("now", $berlinTZ);
if (!empty($_GET)) {
    include 'inc/getFilter.php';
} else {
    if (date('D') === 'Mon') {
        $mon = $curDate->modify('00:00:00');
        $sun = $curDate->modify('next Sunday');
    } else if (date('D') === 'Sun') {
        $mon = $curDate->modify('last Monday');
        $sun = $curDate->modify('23:59:59');
    } else {
        $mon = $curDate->modify('last Monday');
        $sun = $curDate->modify('next Sunday');
    }
    $newDateFrom = $mon->format("Y-m-d H:i:s");
    $newDateTo = $sun->format("Y-m-d H:i:s");

    $sql = 'SELECT posts.*, companies.name AS company_name, users.name AS worker_name, services.name AS service_name, u.name AS creator_name, posts.date_edited, e.name AS editor_name
                           FROM posts LEFT JOIN companies ON posts.company_id = companies.company_id LEFT JOIN users ON posts.worker_id = users.user_id
                LEFT JOIN services ON posts.service_id = services.service_id LEFT JOIN users AS u ON posts.user_id = u.user_id LEFT JOIN users AS e ON posts.person_edited = e.user_id WHERE posts.date_created>=:datepickerFrom AND posts.date_created<=:datepickerTo';

    if (isset($_SESSION['company'])) {
        $sql .= " AND posts.company_id LIKE :companyId";
    }

    $sql .= " ORDER BY posts.date_created ASC";
    $dataQuery = $db->prepare($sql);

    if (isset($_SESSION['company'])) {
        $dataQuery->bindValue(':companyId', $_SESSION['company']);
    }
    $dataQuery->bindValue(':datepickerFrom', $newDateFrom);
    $dataQuery->bindValue(':datepickerTo', $newDateTo);

    $dataQuery->execute();
}

include 'inc/header.php';
include 'inc/navbar.php';
include 'inc/headline.php';
include 'inc/filter.php';

$data = $dataQuery->fetchAll(PDO::FETCH_ASSOC);

echo '<h2 class="py-1 px-2">From:' . htmlspecialchars($newDateFrom ?? 'none') . ' - To:' . htmlspecialchars($newDateTo ?? 'none') . '</h2>';
include 'inc/deleteChecked.php';

if (!empty($data)) {

    $dbCol = $db->prepare('SELECT users.name AS name FROM users ORDER BY name ASC;');
    $dbCol->execute();
    $colnames = $dbCol->fetchAll(PDO::FETCH_ASSOC);

    foreach ($colnames as $key => $col) {
        $colnames[$key] = strtolower($col['name']);
    }

    $query = $db->prepare('SELECT * FROM orders;');
    $query->execute();
    $dbOrder = $query->fetch(PDO::FETCH_ASSOC);
    $spaces = str_replace(' ', '', $dbOrder['order']);
    $order = explode(',', $spaces);

    if (sizeof($order) == sizeof($colnames)) {
        $colnames = $order;
    }

    echo '<div class="table-responsive-xl scrollY">
<table class="table table-striped table-bordered">
    <thead>
    <tr>';
    echo '<th scope="col">#</th>';
    echo '<th scope="col">name</th>';
    echo '<th scope="col">total</th>';
    echo '<th scope="col">commission</th>';
    echo '<th scope="col">Date From</th>';
    echo '<th scope="col">Date To</th>';
    echo '<th scope="col">Delete</th>';
    echo '</tr>

    </thead>
    <tbody>';
    foreach ($colnames as $index => $colname) {
        $total = 0;
        $ids = [];
        foreach ($data as $key => $d) {
            if ($colname == strtolower($d['worker_name'])) {
                $total += $d['cost'];
                $ids[$colname][] = $d['post_id'];
            }
        }
        echo '<tr>
        <th scope="row">' . ++$index . '.</th>
        <td>' . htmlspecialchars($colname ?? '-') . '</td>
        <td>' . htmlspecialchars($total) . '</td>
        <td style="color: red">' . htmlspecialchars($total * 0.5) . '</td>
        <td>' . htmlspecialchars($newDateFrom ?? '-') . '</td>
        <td>' . htmlspecialchars($newDateTo ?? '-') . '</td>
        <td>';
        if (isset($ids[$colname])) {
            echo '<input type="checkbox" class="checkbox" name="ids[]" value="' . implode(", ", array_values($ids[$colname])) . '"></td>';
        }
        echo '</tr>';
    }
    echo '</tbody>
</table>
</div>';
    echo '</form>';

} else {
    echo '
    <div class="alert alert-info">No posts were found.</div>
    ';
}
?>
    <script>
        $(function () {
            $('.check_all').click(function () {
                $('.checkbox').prop('checked', true);
            });
            $('.uncheck_all').click(function () {
                $('.checkbox').prop('checked', false);
            });
        });
    </script>
<?php

echo '<h2 class="py-1 px-2">Data check</h2>';
if (!empty($data)) {
    if ($_SESSION['permission'] == 'secretary') {
        $dbCol = $db->prepare('SELECT companies.name AS company_name, users.name AS worker_name, services.name AS service_name, posts.cost, posts.payment, posts.comment,
       posts.date_created, u.name AS creator_name
                           FROM posts LEFT JOIN companies ON posts.company_id = companies.company_id LEFT JOIN users ON posts.worker_id = users.user_id
        LEFT JOIN services ON posts.service_id = services.service_id LEFT JOIN users AS u ON posts.user_id = u.user_id ORDER BY worker_name ASC;');
    } else if ($_SESSION['permission'] == 'admin') {
        $dbCol = $db->prepare('SELECT companies.name AS company_name, users.name AS worker_name, services.name AS service_name, posts.cost, posts.payment, posts.comment,
       posts.date_created, u.name AS creator_name, posts.date_edited, e.name AS editor_name
                           FROM posts LEFT JOIN companies ON posts.company_id = companies.company_id LEFT JOIN users ON posts.worker_id = users.user_id
        LEFT JOIN services ON posts.service_id = services.service_id LEFT JOIN users AS u ON posts.user_id = u.user_id LEFT JOIN users AS e ON posts.person_edited = e.user_id ORDER BY worker_name ASC;');
    }

    $dbCol->execute();
    $col = $dbCol->fetchAll(PDO::FETCH_ASSOC);

// Fetch column names from the result set
    $colnames = [];
    for ($i = 0; $i < $dbCol->columnCount(); $i++) {
        $colMeta = $dbCol->getColumnMeta($i);
        $colnames[] = $colMeta['name'];
    }
    echo '<div class="table-responsive-xl scrollY">
<table class="table table-striped">
    <thead>
    <tr>
        <th scope="col">#</th>';
    foreach ($colnames as $colname) {
        echo '<th scope="col">' . htmlspecialchars($colname ?? '-') . '</th>';
    }
    echo '</tr>

    </thead>
    <tbody>';
    foreach ($data as $index => $d) {
        echo '<tr>
        <th scope="row">' . ++$index . '.</th>
        <td>' . htmlspecialchars($d['company_name'] ?? '-') . '</td>
        <td>' . htmlspecialchars($d['worker_name'] ?? '-') . '</td>
        <td>' . htmlspecialchars($d['service_name'] ?? '-') . '</td>
        <td>' . htmlspecialchars($d['cost'] ?? '-') . '</td>
        <td>' . htmlspecialchars($d['payment'] ?? '-') . '</td>
        <td>' . htmlspecialchars($d['comment'] ?? '-') . '</td>
        <td>' . htmlspecialchars($d['date_created'] ?? '-') . '</td>
        <td>' . htmlspecialchars($d['creator_name'] ?? '-') . '</td>';
        if ($_SESSION['permission'] == 'admin') {
            echo '<td > ' . htmlspecialchars($d['date_edited'] ?? '-') . ' </td >
                <td > ' . htmlspecialchars($d['editor_name'] ?? '-') . ' </td >';
        }
        echo '</tr>';
    }
    echo '</tbody>
</table>
</div>';
    ?>
    <style>
        .scrollY {
            max-height: 80vh;
            overflow-y: scroll;
        }
    </style>
    <?php
} else {
    echo '
    <div class="alert alert-info">No posts were found.</div>
    ';
}
//vložíme do stránek patičku
include 'inc/footer.php';
