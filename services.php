<?php
$pageTitle = 'Services';
require_once 'inc/userCheck.php';
require_once 'inc/db.php';
/** @var \PDO $db */

$errors = [];
$name = '';
$cost = '';
if (!empty($_POST)) {
    if (isset($_POST['delete']) && isset($_POST['confirmSingleFlag'])) {
        if ($_POST['confirmSingleFlag'] === 'true') {
            $deleteQuery = $db->prepare("DELETE FROM services WHERE services.service_id=:serviceId;");
            $deleteQuery->execute([
                ':serviceId' => $_POST['delete']
            ]);
            $_SESSION['operation'] = 'success';
            $_SESSION['announce'] = 'Service deleted.';
        }

    } else if (isset($_POST['deleteChecked'])) {
        if (isset($_POST['ids']) && isset($_POST['confirmFlag'])) {
            if ($_POST['confirmFlag'] === 'true') {
                foreach ($_POST['ids'] as $el) {
                    $deleteQuery = $db->prepare("DELETE FROM services WHERE services.service_id=:serviceId;");
                    $deleteQuery->execute([
                        ':serviceId' => $el
                    ]);
                }
                $_SESSION['operation'] = 'success';
                $_SESSION['announce'] = 'Checked Services deleted.';
            }
        } else {
            $_SESSION['operation'] = 'fail';
            $_SESSION['announce'] = 'No Services checked, nothing to delete.';
        }

    } else {
        if (!empty($_POST['name'])) {
            $name = trim(@$_POST['name']);
            if (!preg_match("/^[a-zA-Z0-9 ]{2,}$/",$_POST['name']) ) {
                $errors['name'] = 'Service name must be at least 2 letters long please and must contain only letters and numbers.';
            }
        } else {
            $errors['name'] = 'Type in service full name please.';
        }

        if (!empty($_POST['cost']) || $_POST['cost'] == '0') {
            $cost = trim(@$_POST['cost']);
            if (!preg_match('/^-?\d{1,4}$/', $cost)) {
                $errors['cost'] = "Incorrect cost format. Must be between -9999 and 9999.";
            }
        } else {
            $errors['cost'] = 'Type in default cost please.';
        }

        if (empty($errors)) {
            if (isset($_GET['action'])) {
                if ($_GET['action'] == 'edit' && isset($_GET['serviceId']) && is_numeric($_GET['serviceId'])) {
                    $slctQuery = $db->prepare("SELECT * FROM services WHERE services.service_id=:serviceId;");
                    $slctQuery->execute([
                        ':serviceId' => $_GET['serviceId']
                    ]);
                    if ($slctQuery->rowCount() == 0) {
                        $_SESSION['operation'] = 'fail';
                        $_SESSION['announce'] = 'This Service doesnt exist!';
                        header('Location: service.php');
                        exit();
                    } else {
                        $editQuery = "UPDATE services SET";

                        if (!empty($_POST['name'])) {
                            $editQuery .= " services.name=:name";
                        }

                        if (!empty($_POST['cost']) || $_POST['cost'] == 0) {
                            $editQuery .= ", default_cost=:cost";
                        }

                        $editQuery .= " WHERE services.service_id=:serviceId;";

                        $editSql = $db->prepare($editQuery);

                        if (!empty($name)) {
                            $editSql->bindValue(':name', $name);
                        }

                        if (!empty($cost) || $cost == 0) {
                            $editSql->bindValue(':cost', $cost);
                        }

                        if (!empty($_GET['serviceId'])) {
                            $editSql->bindValue(':serviceId', $_GET['serviceId']);
                        }

                        $editSql->execute();

                        $_SESSION['operation'] = 'success';
                        $_SESSION['announce'] = 'Service edited successfully.';
                        header('Location: services.php');
                        exit();
                    }
                } else {
                    $_SESSION['operation'] = 'fail';
                    $_SESSION['announce'] = 'Undefined action, Services page refreshed.';
                    header('Location: services.php');
                    exit();
                }
            } else {
                $editQuery = "INSERT INTO services (";

                if (!empty($_POST['name'])) {
                    $editQuery .= "services.name";
                }

                if (!empty($_POST['cost']) || $_POST['cost'] == 0) {
                    $editQuery .= ", default_cost";
                }

                $editQuery .= ") VALUES (";

                if (!empty($_POST['name'])) {

                    $editQuery .= ":name";
                }

                if (!empty($_POST['cost']) || $_POST['cost'] == 0) {

                    $editQuery .= ", :cost";
                }

                $editQuery .= ");";
                $editSql = $db->prepare($editQuery);

                if (!empty($name)) {
                    $editSql->bindValue(':name', $name);
                }

                if (!empty($cost) || $cost == 0) {
                    $editSql->bindValue(':cost', $cost);
                }

                $editSql->execute();

                $_SESSION['operation'] = 'success';
                $_SESSION['announce'] = 'A new Service was successfully created.';
                header('Location: services.php');
                exit();
            }
        }
    }
}

if (isset($_GET['action'])) {
    if ($_GET['action'] == 'edit' && !empty($_GET['serviceId'])) {
        if (is_numeric($_GET['serviceId'])) {
            $query = $db->prepare('SELECT * FROM services WHERE service_id=:serviceId LIMIT 1;');
            $query->execute([
                ':serviceId' => $_GET['serviceId']
            ]);
            $value = $query->fetch(PDO::FETCH_ASSOC);
            if (empty($value)) {
                $_SESSION['operation'] = 'fail';
                $_SESSION['announce'] = 'Service for editing doesn\'t exist.';
                header('Location: services.php');
                exit();
            }
        } else {
            $_SESSION['operation'] = 'fail';
            $_SESSION['announce'] = 'Service for editing doesn\'t exist.';
            header('Location: services.php');
            exit();
        }
    } else {
        $_SESSION['operation'] = 'fail';
        $_SESSION['announce'] = 'Undefined action, Services page refreshed.';
        header('Location: services.php');
        exit();
    }
}

include 'inc/header.php';
include 'inc/headline.php';

?>
    <form method="post">
    <div class="row no-gutters">
        <?php
        echo '<a href="index.php" class="btn btn-secondary">Back</a>';
        if (!empty($value)) {
            echo '<span class="font-weight-bold" style="align-self: center;">Currently editing - ' . htmlspecialchars($value['name']) . '</span>';
        }
        ?>
    </div>

        <div class="form-group">
            <label for="name">Name</label>
            <?php
            if (!empty($value) && empty($name) && empty($errors['name'])) {
                echo '<input type="text" maxlength="50" class="form-control" id="name" name="name" value="' . htmlspecialchars($value['name'] ?? '') . '" autocomplete="off">';
            } else {
                echo '<input type="text" maxlength="50" class="form-control ' . (!empty($errors['name']) ? 'is-invalid' : '') . '" id="name" name="name" value="' . htmlspecialchars($name ?? '') . '" placeholder="Service name(max 50 characters long)">';
            }
            if (!empty($errors['name'])) {
                echo '<div class="invalid-feedback" style="display: flex">' . $errors['name'] . '</div>';
            }
            ?>
        </div>

        <div class="form-group">
            <label for="cost" id="costLabel">Default cost</label>
            <?php
            if (!empty($value) && empty($cost) && empty($errors['cost'])) {
                echo '<input type="text" maxlength="4" class="form-control" id="cost" name="cost" onchange=checkCost() value="' . htmlspecialchars($value['default_cost'] ?? '') . '" autocomplete="off">';
            } else {
                echo '<input type="text" maxlength="4" class="form-control ' . (!empty($errors['cost']) ? 'is-invalid' : '') . '" id="cost" name="cost" value="' . htmlspecialchars($cost ?? '') . '" placeholder="Type in service\'s default cost">';
            }
            if (!empty($errors['cost'])) {
                echo '<div class="invalid-feedback" style="display: flex">' . $errors['cost'] . '</div>';
            }
            ?>
        </div>
        <button type="submit" class="btn btn-primary" name="process">Submit</button>
    </form>

<?php
$query = $db->prepare('SELECT * FROM services ORDER BY name DESC;');
$query->execute();
$dbServices = $query->fetchAll(PDO::FETCH_ASSOC);

echo '<h2 class="py-1 px-2">Services</h2>';
include 'inc/deleteChecked.php';

if (!empty($dbServices)) {
    echo '<div class="row no-gutters justify-content-center scrollY">';
    foreach ($dbServices as $index => $service) {
        echo '<article class="col col-sm-6 col-md-5 col-lg-4 col-xl-3 border border-dark mx-1 my-1 px-2 py-1">';
        echo '<div class="d-flex justify-content-between">
            <div>
                <span class="font-weight-bold">' . ($index + 1) . '.</span>
                <span class="badge badge-info">' . htmlspecialchars($service['name']) . '</span>
            </div>
            
            <input type="checkbox" class="checkbox" name="ids[]" value="'.$service['service_id'].'">
            </div>';

        echo '<div class="d-flex">
                <label>Default Cost: </label>
                <div class="input-group" style="margin-left: 2px; min-width:120px; max-width:120px">
                    <div class="input-group-prepend">
                        <span class="input-group-text">€</span>
                    </div>
                    <span class="form-control" id="costs">' . htmlspecialchars($service['default_cost']) . '</span>
                </div>
                </div>';

        echo '<div class="row no-gutters justify-content-between align-bottom">
                <a href="services.php?action=edit&serviceId=' . $service['service_id'] . '" class="btn btn-light" style="position:relative;left:4%">Edit service</a>
                <button type="submit" class="btn btn-danger" onclick="return confirmSingleDelete()" name="delete" id="delete'.$index.'" value="' . $service['service_id'] . '">Delete</button>
              </div>';
        echo '</article>';
    }
    echo '</div>';
    echo '<input type="hidden" name="confirmSingleFlag" id="confirmSingleFlag" value="">';
    echo '</form>';
} else {
    echo '<div class="alert alert-info">No services were found.</div>';
}

?>
    <script>
        function confirmSingleDelete() {
            if (confirm("Delete Single Service?")) {
                document.getElementById('confirmSingleFlag').value = 'true';
            } else {
                document.getElementById('confirmSingleFlag').value = 'false';
            }
        }

        $(function() {
            $('.check_all').click(function() {
                $('.checkbox').prop('checked', true);
            });
            $('.uncheck_all').click(function() {
                $('.checkbox').prop('checked', false);
            });
        });
    </script>


    <style>
        .scrollY {
            max-height: 80vh;
            overflow-y: scroll;
        }
    </style>

<?php

include 'inc/footer.php';
