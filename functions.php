<?php
$pageTitle = "Download";
require_once 'inc/userCheck.php';
require_once 'inc/db.php';
/** @var \PDO $db */

if (isset($_POST["Import"]) && !empty($_GET['tableName'])) {
    $filename = $_FILES["file"]["tmp_name"];
    if ($_FILES["file"]["size"] > 0) {

        $file = fopen($filename, "r");

        $query = $db->prepare('SELECT DISTINCT table_name FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA=\'name\';');
        $query->execute();
        $dbTables = $query->fetchAll(PDO::FETCH_ASSOC);
        foreach ($dbTables as $key => $col) {
            $dbTables[$key] = $col['table_name'];
        }
        $tableName = trim($_GET['tableName']);
        if (!in_array($tableName, $dbTables)) {
            header('Location: download.php?tableName=posts');
            exit();
        }

        if ($file !== false) {
            $getData = fgetcsv($file, 0, ";");

            $query = $db->prepare('SELECT column_name FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = :tableName AND TABLE_SCHEMA=\'name\';');
            $query->execute([
                ':tableName'=>$tableName
            ]);
            $colnames = $query->fetchAll(PDO::FETCH_ASSOC);
            foreach ($colnames as $key => $col) {
                $colnames[$key] = $col['column_name'];
            }
            /*$deleteIds = explode(", ", $userId);
            $placeholders = implode(', ', array_fill(0, count($deleteIds), '?'));
            $deleteQuery = $db->prepare("DELETE FROM posts WHERE post_id IN ($placeholders)");
            $deleteQuery->execute($deleteIds);*/

            $cols = implode(',', $colnames);
            $insertCols = explode(',', $cols);
            $placeholders = implode(',', array_fill(0, count($insertCols), '?'));
            echo "<br>$placeholders<br>";
            while (($getData = fgetcsv($file, 0, ";")) !== false) {
                if (count($getData) === count($colnames)) {
                    $sql = "INSERT INTO $tableName(";

                    $sql .= "$cols" . ") VALUES ($placeholders);";

                    $query = $db->prepare($sql);
                    $result = $query->execute($getData);

                    if (!$result) {
                        echo "Error inserting data: " . implode(", ", $query->errorInfo()) . "<br>";
                        echo '<a href="download.php?tableName=' . htmlspecialchars($tableName) . '" class="btn btn-light">Go back</a>';
                    } else {
                        echo "<script type=\"text/javascript\">
                            alert(\"CSV File has been successfully Imported.\");
                            window.location = \"download.php?tableName=$tableName\"
                            </script>";
                    }
                } else {
                    echo "Invalid data: " . implode(", ", $getData) . "<br>";
                    echo '<a href="download.php?tableName=' . htmlspecialchars($tableName) . '" class="btn btn-light">Go back</a>';
                }
            }

            fclose($file);
        } else {
            echo "Error opening file.";
            echo '<a href="download.php?tableName=' . htmlspecialchars($tableName) . '" class="btn btn-light">Go back</a>';
        }
    }
}

if (isset($_POST["Export"])) {
    if (!empty($_GET['tableName'])) {
        $tableName = trim($_GET['tableName']);

        header('Content-Type: text/csv; charset=utf-8');
        header('Content-Disposition: attachment; filename=data.csv');
        $output = fopen("php://output", "w");

        $query = $db->prepare('SELECT column_name FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = :tableName AND TABLE_SCHEMA=\'name\';');
        $query->execute([
            ':tableName'=>$tableName
        ]);
        $colnames = $query->fetchAll(PDO::FETCH_ASSOC);
        foreach ($colnames as $key => $col) {
            $colnames[$key] = $col['column_name'];
        }
        fputcsv($output, $colnames, ';');

        $result = $db->prepare("SELECT * from $tableName;");
        $result->execute();
        $row = $result->fetchAll(PDO::FETCH_ASSOC);
        foreach ($row as $r) {
            fputcsv($output, $r, ';');
        }
        fclose($output);
    } else {
        echo '<div class="alert alert-info">Please select table to export data.</div>';
    }

}