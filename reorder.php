<?php
$pageTitle = 'Reorder';
require_once 'inc/userCheck.php';
require_once 'inc/db.php';
/** @var \PDO $db */

$errors = [];
$postOrder = [];
$order = '';
if (!empty($_POST)) {
    if (!empty($_POST['order'])) {
        $name = trim(@$_POST['order']);
        $spaces = str_replace(' ', '', $_POST['order']);
        $spaces = strtolower($spaces);
        $postOrder = explode(',', $spaces);
        $duplicates = (count($postOrder) !== count(array_unique($postOrder)));

        $dbCol = $db->prepare('SELECT users.name AS name FROM users ORDER BY name ASC;');
        $dbCol->execute();
        $users = $dbCol->fetchAll(PDO::FETCH_ASSOC);

        foreach ($users as $key => $user) {
            $users[$key] = strtolower($user['name']);
        }


        if (sizeof($postOrder) > sizeof($users)) {
            $errors['order'] = 'There are too many names than there exists! Type in order in this format: e.g. admin, secretary, worker';
        } else if ($duplicates) {
            $errors['order'] = 'There are duplicate names in your order!';
        } else {
            foreach ($postOrder as $d) {
                $bool = in_array($d, $users);
                if ($bool === false) {
                    $errors['order'] = 'This name: ' . $d . ' doesn\'t exist in users. Type in order in this format: e.g. admin, secretary, worker';
                    break;
                }
            }

            if (empty($errors['order'])) {
                $newOrder = array_merge($postOrder,$users);
                $newOrder = array_unique($newOrder);
                $newOrder = implode(',',$newOrder);
            }
        }

    } else {
        $errors['order'] = 'Order is empty.';
    }


    if (empty($errors)) {
        $editSql = $db->prepare('UPDATE orders SET orders.order=:order;');
        $editSql->execute([
            ':order'=>$newOrder
        ]);

        $_SESSION['operation'] = 'success';
        $_SESSION['announce'] = 'Order edited successfully.';
    }
}

include 'inc/header.php';
include 'inc/headline.php';

?>
    <form method="post">
        <div class="row no-gutters">
            <?php
            $query = $db->prepare('SELECT * FROM orders;');
            $query->execute();
            $value = $query->fetch(PDO::FETCH_ASSOC);
            if (empty($value)) {
                $_SESSION['operation'] = 'fail';
                $_SESSION['announce'] = 'Failed to obtain order from database. Returned to Dashboard.';
                header('Location: index.php');
                exit();
            }
            echo '<a href="index.php" class="btn btn-secondary">Back</a>';
            ?>
        </div>

        <div class="form-group">
            <div class="form-row">
                <label for="order">Order</label>
                <div class="text-muted font-italic">&nbsp;(Type each account name in the order you want it displayed in Commission and Scheduler pages)</div>
            </div>
            <?php
            if (!empty($_POST)) {
                echo '<textarea class="form-control ' . (!empty($errors['order']) ? 'is-invalid' : '') . '" maxlength="200" name="order" id="order" >' . htmlspecialchars($_POST['order'] ?? '') . '</textarea>';
            } else {
                echo '<textarea class="form-control" maxlength="200" name="order" id="order" >' . htmlspecialchars($value['order'] ?? '') . '</textarea>';
            }
            if (!empty($errors['order'])) {
                echo '<div class="invalid-feedback" style="display: flex">' . $errors['order'] . '</div>';
            }
            ?>
        </div>
        <?php
        $query = $db->prepare('SELECT * FROM orders;');
        $query->execute();
        $value = $query->fetch(PDO::FETCH_ASSOC);
        ?>
        <button type="submit" class="btn btn-primary" name="process">Submit</button>
    </form>

<?php
$query = $db->prepare('SELECT * FROM users ORDER BY date_created DESC;');
$query->execute();
$accounts = $query->fetchAll(PDO::FETCH_ASSOC);

$dbCol = $db->prepare('SELECT users.name AS name FROM users ORDER BY name ASC;');
$dbCol->execute();
$colnames = $dbCol->fetchAll(PDO::FETCH_ASSOC);

foreach ($colnames as $key => &$col) {
    $col = strtolower($col['name']);
}

$query = $db->prepare('SELECT * FROM orders;');
$query->execute();
$dbOrder = $query->fetch(PDO::FETCH_ASSOC);
$spaces = str_replace(' ', '', $dbOrder['order']);
$order = explode(',', $spaces);

if (sizeof($order) == sizeof($colnames)) {
    $colnames = $order;
}

echo '<h2 class="py-1 px-2">Order of Accounts</h2>';
echo '<div class="row no-gutters">';
if (!empty($accounts) && !empty($colnames)) {
    $index = 1;
    foreach ($colnames as $colname) {
        foreach ($accounts as $key => $account) {
            if ($_SESSION['permission'] != 'admin' && $account['permission'] == 'admin') {
                break;
            }
            if ($colname == strtolower($account['name'])) {

                echo '<article class="col col-sm-6 col-md-5 col-lg-4 col-xl-3 border border-dark mx-1 my-1 px-2 py-1">';
                echo '<div class="d-flex justify-content-between">
            <div>
                <span class="font-weight-bold">' . $index++ . '.</span>
                <span class="badge badge-warning">' . htmlspecialchars($account['name']) . '</span>
            </div>
           
        </div>';

                echo '<div class="badge badge-dark mb-1">';
                if ($_SESSION['permission'] == 'secretary') {
                    htmlspecialchars($account['permission'] ?? 'Boss');
                } else if ($_SESSION['permission'] == 'admin') {
                    htmlspecialchars($account['permission'] ?? 'error');
                }
                echo '</div>';

                echo '<div class="d-flex">
                    <div class="small text-muted">';
                echo ' ';
                echo '<div>' . date('d.m.Y H:i:s', strtotime($account['date_created'])) . '</div>';
                echo ' ';
                echo '</div>
            </div>';
                echo '</article>';

                unset($accounts[$key]);
                break;
            }
        }
    }
    echo '</div>';
} else {
    echo '<div class="alert alert-info">No accounts were found.</div>';
}

include 'inc/footer.php';
