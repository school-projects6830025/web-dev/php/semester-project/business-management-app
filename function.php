<?php
$pageTitle = "Download";
require_once 'inc/userCheck.php';
require_once 'inc/db.php';
/** @var \PDO $db */
function get_all_records(): void {
    if (!empty($_GET['tableName'])) {
        global $db;
        global $dbTables;

        $tableName = trim($_GET['tableName']);
        if (!in_array($tableName, $dbTables)) {
            header('Location: download.php?tableName=posts');
            exit();
        }

        $query = $db->prepare("SELECT * FROM $tableName;");
        $query->execute();

        if ($query->rowCount() > 0) {
            $colQuery = $db->prepare("SELECT column_name FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name=:tableName AND TABLE_SCHEMA='name';");
            $colQuery->execute([
                ':tableName'=>$tableName
            ]);
            $colnames = $colQuery->fetchAll(PDO::FETCH_ASSOC);
            foreach ($colnames as $key => $col) {
                $colnames[$key] = $col['column_name'];
            }

            echo "<div class='table-responsive-xl scrollY'>
                <table class='table table-striped table-bordered'>
             <thead>
                <tr>";
            foreach ($colnames as $colname) {
                echo '<th>' . htmlspecialchars($colname) . '</th>';
            }
            echo "</tr>
             </thead>
             <tbody>";
            $result = $query->fetchAll(PDO::FETCH_ASSOC);
            foreach ($result as $row) {
                echo "<tr>";
                foreach ($colnames as $colname) {
                    echo '<td>' . htmlspecialchars($row[$colname] ?? 'null') . '</td>';
                }
                echo "</tr>";
            }

            echo "</tbody></table></div>";
            ?>
            <style>
                .scrollY {
                    max-height: 80vh;
                    overflow-y: scroll;
                }
            </style>

        <?php
        } else {
            echo '<div class="alert alert-info">No records were found.</div>';
        }
    } else {
        echo '<div class="alert alert-info">Please select table to display.</div>';
    }
}
?>