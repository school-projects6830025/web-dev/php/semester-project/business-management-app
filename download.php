<?php
/** Basic page for exporting and importing of data. */
$pageTitle = "Download";
require_once 'inc/userCheck.php';
include_once 'function.php';
include_once 'functions.php';

require_once 'inc/db.php';
/** @var \PDO $db */

$query = $db->prepare('SELECT DISTINCT table_name FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA=\'name\';');
$query->execute();
$dbTables = $query->fetchAll(PDO::FETCH_ASSOC);
foreach ($dbTables as $key => $col) {
    $dbTables[$key] = $col['table_name'];
}

if (!empty($_GET['tableName'])) {
    $_GET['tableName'] = trim($_GET['tableName']);

    if (!in_array($_GET['tableName'], $dbTables)) {
        header('Location: download.php?tableName=posts');
        exit();
    }
} else {
    $_GET['tableName'] = 'posts';
}

include 'inc/header.php';
include 'inc/navbar.php';
include 'inc/headline.php';
?>

<form method="get">
    <div class="form-group">
        <label for="tableName">Choose data:</label>
        <select class="form-control" id="tableName" name="tableName">
            <?php
            foreach ($dbTables as $tableName) {
                echo '<option value="' . $tableName . '" ' . (($_GET['tableName'] == $tableName) ? 'selected' : '') . '>' . htmlspecialchars($tableName) . '</option>';
            }
            ?>
        </select>
        <a href="index.php" class="btn btn-secondary">Back</a>
        <button type="submit" class="btn btn-primary">Filter</button>
    </div>
</form>


<div class="row no-gutters justify-content-between">
    <form class="form-row" action="functions.php?tableName=<?php echo $_GET['tableName'] ?>" method="post"
          name="upload_excel"
          enctype="multipart/form-data">
        <fieldset>
            <legend style="font-weight: 800"><?php echo htmlspecialchars(ucfirst($_GET['tableName']) ?? '') ?></legend>
            <div class="form-row">
                <label class="col-4 col-sm-3 control-label" for="filebutton">Select File</label>
                <div class="col col-sm-1">
                    <input type="file" name="file" id="file" class="input-large">
                </div>
            </div>
            <div class="form-row">
                <div class="col-md-4">
                    <button type="submit" id="submit" name="Import" class="btn btn-primary my-1"
                            data-loading-text="Loading...">Import file
                    </button>
                </div>
            </div>
        </fieldset>
    </form>

    <form class="form-row" action="functions.php?tableName=<?php echo $_GET['tableName'] ?>" method="post"
          name="upload_excel"
          enctype="multipart/form-data">
        <div class="form-group">
            <input type="submit" name="Export" class="btn btn-success mt-4" value="export to excel"/>
        </div>
    </form>
</div>


<?php
get_all_records();

include 'inc/footer.php';
?>
