<?php
$pageTitle = "Scheduler";
require_once 'inc/userCheck.php';
require_once 'inc/db.php';
/** @var \PDO $db */

$berlinTZ = new DateTimeZone("Europe/Berlin");
$curDate = new DateTimeImmutable("now", $berlinTZ);

if (!empty($_GET)) {
    $sql = "SELECT posts.*, companies.name AS company_name, users.name AS worker_name, services.name AS service_name, u.name AS creator_name
                           FROM posts LEFT JOIN companies ON posts.company_id = companies.company_id LEFT JOIN users ON posts.worker_id = users.user_id
        LEFT JOIN services ON posts.service_id = services.service_id LEFT JOIN users AS u ON posts.user_id = u.user_id WHERE 1";

        $date = DateTime::createFromFormat('d-m-Y H:i:s', $_GET['date']);
        $tmp = $date->setTime(00, 00, 00);
        $newDateFrom = $tmp->format('Y-m-d H:i:s');
        $tmp = $date->setTime(23, 59, 59);
        $newDateTo = $tmp->format('Y-m-d H:i:s');
        $sql .= " AND posts.date_created>=:datepickerFrom";
        $sql .= " AND posts.date_created<=:datepickerTo";

    if (!empty($_GET['company'])) {
        $sql .= " AND (posts.company_id LIKE :companyId OR (:companyId = '%' AND posts.company_id IS NULL))";
    }

    $sql .= " ORDER BY posts.date_created ASC;";
    $dataQuery = $db->prepare($sql);

    if (!empty($_GET['company'])) {
        $dataQuery->bindValue(':companyId', $_GET['company']);
    }

        $dataQuery->bindValue(':datepickerFrom', $newDateFrom);

        $dataQuery->bindValue(':datepickerTo', $newDateTo);


    $dataQuery->execute();
    $_SESSION['operation'] = 'success';
    $_SESSION['announce'] = 'Data filtered.';
} else {
    $tmp = $curDate->setTime(00, 00, 00);
    $newDateFrom = $tmp->format('Y-m-d H:i:s');
    $tmp = $curDate->setTime(23, 59, 59);
    $newDateTo = $tmp->format('Y-m-d H:i:s');

    $sql = 'SELECT
                           posts.*, companies.name AS company_name, users.name AS worker_name, services.name AS service_name, u.name AS creator_name
                           FROM posts LEFT JOIN companies ON posts.company_id = companies.company_id LEFT JOIN users ON posts.worker_id = users.user_id
                LEFT JOIN services ON posts.service_id = services.service_id LEFT JOIN users AS u ON posts.user_id = u.user_id WHERE posts.date_created>=:datepickerFrom AND posts.date_created<=:datepickerTo';

    if (isset($_SESSION['company'])) {
        $sql .= " AND (posts.company_id LIKE :companyId OR (:companyId = '%' AND posts.company_id IS NULL))";
    }

    $sql .= " ORDER BY posts.date_created ASC";
    $dataQuery = $db->prepare($sql);

    if (isset($_SESSION['company'])) {
        $dataQuery->bindValue(':companyId', $_SESSION['company']);
    }
    $dataQuery->bindValue(':datepickerFrom', $newDateFrom);
    $dataQuery->bindValue(':datepickerTo', $newDateTo);

    $datepicker = $tmp->format('d-m-Y H:i:s');
    $_GET['date'] = $datepicker;
    $dataQuery->execute();
}

include 'inc/header.php';
include 'inc/navbar.php';
include 'inc/headline.php';
?>


<?php

$query = $db->prepare('SELECT * FROM companies ORDER BY name;');
$query->execute();
$dbCompanies = $query->fetchAll(PDO::FETCH_ASSOC);
echo '<form method="get" style="z-index: 1">
<div class="form-row">
        <div class="form-group col-8 col-sm-5 col-md-4 col-lg-3 col-xl-3" style="min-width: 145px">
                <div class="d-flex flex-column">
                    <label for="company" style="margin-bottom: 8px">Company</label>';
if (!empty($dbCompanies)) {
    echo '<div class="btn-group btn-group-toggle" data-toggle="buttons">';
    if (isset($_GET['company'])) {
        echo '<label class="btn btn-secondary ' . (($_GET['company'] == "%") ? 'active' : '') . '" data-toggle="buttons">
                            <input type="radio" name="company" id="company" autocomplete="off" value="%" ' . (($_GET['company'] == "%") ? 'checked' : '') . '>All
                        </label>';
        foreach ($dbCompanies as $index => $dbCompany) {
            echo '<label class="btn btn-secondary ' . (($_GET['company'] == $dbCompany['company_id']) ? 'active' : '') . '">
                                    <input type="radio" name="company" id="company'.$index.'" autocomplete="off" value="' . $dbCompany['company_id'] . '" ' . (($_GET['company'] == $dbCompany['company_id']) ? 'checked' : '') . '> ' . htmlspecialchars($dbCompany['name'] ?? '') . '
                                </label>';
        }
    } else {
        echo '<label class="btn btn-secondary ' . ((isset($_SESSION['company']) && $_SESSION['company'] == "%") ? 'active' : '') . '" data-toggle="buttons">
                            <input type="radio" name="company" id="company" autocomplete="off" value="%" ' . ((isset($_SESSION['company']) && $_SESSION['company'] == "%") ? 'checked' : '') . '>All
                                </label>';
        foreach ($dbCompanies as $index => $dbCompany) {
            echo '<label class="btn btn-secondary ' . ((isset($_SESSION['company']) && $_SESSION['company'] == $dbCompany['company_id']) ? 'active' : '') . '">
                                <input type="radio" name="company" id="company'.$index.'" autocomplete="off" value="' . $dbCompany['company_id'] . '" ' . ((isset($_SESSION['company']) && $_SESSION['company'] == $dbCompany['company_id']) ? 'checked' : '') . '> ' . htmlspecialchars($dbCompany['name'] ?? '') . '
                            </label>';
        }
    }
}
?>
<script>
    $(document).ready(function(){
        $(".btn-group-toggle label").click(function() {
            $(this).siblings().removeClass('active');
            $(this).addClass('active');
            $(this).parent().find('input[type="radio"]').prop('checked', false);
            $(this).find('input[type="radio"]').prop('checked', true);
        });
    });
</script>
<?php
echo '</div>
</div>
</div>';

        echo '<div class="form-group col col-sm col-md col-lg col-xl" style="min-width: 180px">
                <label for="date">Date</label>
                <div class="input-group">';
                echo '<input type="text" onfocus="this.blur()" id="date" name="date" class="form-control" value="' . htmlspecialchars($_GET['date'] ?? '') . '">';
                //echo '<input type="date" id="date" name="date" class="form-control" value="' . htmlspecialchars($_GET['date'] ?? '') . '" placeholder="Date"/>';
                echo '<div class="input-group-append">
                        <button type="button" id="clear" class="btn btn-outline-secondary">&#x2715;</button>
                    </div>
                </div>
            </div>
</div>

        <script>
            $(document).ready(function () {
                $(\'#date\').datepicker({ dateFormat: \'dd-mm-yy 00:00:00\' });
                
                $(\'#clear\').click(function() {
                    $(\'#date\').val(\'\');
                });
            });
        </script>

        <button type="submit" class="btn btn-primary">Filter</button>
    </form>';
?>

<?php

$postHeight = 24;
$postWidth = 149;
$headingHeight = 60;
$rowHeight = 120;
$columnWidth = 150;
$hourWidth = 50;
$schedulerBegin = 10;
$schedulerEnd = 19;

$data = $dataQuery->fetchAll(PDO::FETCH_ASSOC);
if (!empty($data)) {
    $dbCol = $db->prepare('SELECT users.name AS name FROM users ORDER BY name ASC;');
    $dbCol->execute();
    $colnames = $dbCol->fetchAll(PDO::FETCH_ASSOC);

    foreach ($colnames as $key => $col) {
        $colnames[$key] = strtolower($col['name']);
    }

    $query = $db->prepare('SELECT * FROM orders;');
    $query->execute();
    $dbOrder = $query->fetch(PDO::FETCH_ASSOC);
    $spaces = str_replace(' ', '', $dbOrder['order']);
    $order = explode(',', $spaces);

    if (sizeof($order) == sizeof($colnames)) {
        $colnames = $order;
    }

    $posts = [];
    $total = 0;
    echo '<h2 class="py-1 px-2">From:' . htmlspecialchars($newDateFrom ?? '99-99') . ' - To:' . htmlspecialchars($newDateTo ?? '99-99') . '</h2>';
    echo '<div class="scheduler">';
    echo '<div class="colLane">';
    echo '<div class="thead">
                <div>time</div>
</div>';
    foreach ($colnames as $colname) {
        $total = 0;
        foreach ($data as $key => $d) {
            if ($colname == strtolower($d['worker_name'])) {
                $total += $d['cost'];
                $posts[$colname][$key] = $d;
                unset($data[$key]);
            }
        }
        echo '<div class="thead">
                <div class="badge badge-warning">' . htmlspecialchars($colname ?? 'null') . '</div>
                <div class="badge badge-pill badge-light">total: '.$total.'€</div>
                <div class="badge badge-pill badge-primary">commission: '.$total*0.5.'€</div>
            </div>';
    }
    echo '</div>';

    echo '<div class="d-flex align-items-start">';
    echo '<div class="flexLanes"></div>';
    foreach ($colnames as $colname) {
        echo '<div class="flexLanes">';
        if (!empty($posts[$colname])) {
            $change = 0;
            for ($hour = 0, $schedulerBeginTmp = $schedulerBegin ; $schedulerBeginTmp <= $schedulerEnd; $schedulerBeginTmp++, $hour++) {
                $offset = 0;
                foreach ($posts[$colname] as $index => $post) {
                    $postTime = DateTime::createFromFormat('Y-m-d H:i:s', $post['date_created']);
                    $postHour = $postTime->format('G');
                    if ($postHour > $schedulerBeginTmp) {
                        break;
                    }

                    if ($postHour == $schedulerBeginTmp) {
                        $postMinute = $postTime->format('i');

                        if ( ($offset + $postHeight) > ($rowHeight * $hour + 2 * $postMinute) ) {
                            $offset = $offset + ($postHeight/2);
                        } else {
                            $offset = $rowHeight * $hour + 2 * $postMinute;
                        }

                        echo '<div class="post" style="top: ' . $offset . 'px">' . $postTime->format('H:i') . ' - ' . ((!empty($post['service_name'])) ? mb_strimwidth(htmlspecialchars($post['service_name']), 0, 17, "...") : 'None') . ' : ' . htmlspecialchars($post['cost'] . '€' ?? '') . '</div>';
                        unset($posts[$colname][$index]);
                    }
                }

            }
        }
        echo '</div>';

    }
    echo '</div>';
    echo '<div class="hours">';
    $date = DateTime::createFromFormat('H:i', $schedulerBegin-1 . ":00");
    for ($schedulerBeginTmp = $schedulerBegin ; $schedulerBeginTmp <= $schedulerEnd; $schedulerBeginTmp++) {
        $newDate = $date->add(new DateInterval('PT1H'));
        $newDateFormat = $newDate->format('H:i');
        echo '<div>' . htmlspecialchars($newDateFormat ?? '') . '</div>';
    }
    echo '
          </div>';
    echo '</div>';
} else {
    echo '<h2 class="py-1 px-2">From:' . htmlspecialchars($newDateFrom ?? '99-99') . ' - To:' . htmlspecialchars($newDateTo ?? '99-99') . '</h2>
        <div class="alert alert-info">No posts were found.</div>';
}
?>
<style>
    .flexLanes:first-child {
        visibility: hidden;
        width: <?php echo $hourWidth ?>px;
        border: 0;
    }

    .flexLanes {
        display: block;
        position: sticky;
        top: 0;
        width: <?php echo $columnWidth ?>px;
        height: 0;
        margin-top: 0px;
        z-index: 1;
    }

    .flexLanes > div {
        width: <?php echo $columnWidth ?>px;
        font-size: 11px;
        white-space: nowrap;
    }

    .flexLanes > div.post {
        width: <?php echo $postWidth ?>px;
        height: <?php echo $postHeight ?>px;
        background-color: #9ecdff;
        border: 1px solid dodgerblue;
        position: absolute;
    }

    .scheduler {
        position: relative;
        display: inline-block;
        width: 100%;
        overflow-x: auto;
        max-height: 80vh;
        overflow-y: scroll;
        border: 3px solid black;
    }

    <?php $posH = $curDate->format('G');
            $posM = $curDate->format('i');
            ?>
    .scheduler:before {
        position: absolute;
        left: 0;
        top: <?php
            if ($posH >= $schedulerBegin && $posH <= $schedulerEnd) {
                $pos = $headingHeight + $rowHeight * ($posH - $schedulerBegin) + 2 * $posM;
                echo $pos;
            }
            ?>px;
        width: <?php echo sizeof($colnames)*$columnWidth+$hourWidth; ?>px;
        height: 2px;
        background: #c00;
        content: "";
    }

    .thead {
        position: relative;
        display: flex;
        flex-direction: column;
        align-items: center;
        justify-items: center;
    }

    .thead>div {
        position: relative;
        display: table;
        margin: 0 auto;
    }

    .colLane {
        display: block;
        position: sticky;
        height: <?php echo $headingHeight ?>px;
        top: 0;
        vertical-align: top;
        z-index: 2;
    }

    .colLane > div {
        display: inline-block;
        position: sticky;
        top: 0;
        height: <?php echo $headingHeight ?>px;
        width: <?php echo $columnWidth ?>px;
        border-bottom: 1px solid black;
        background-color: #ffd0d0;
        align-content: center;
        line-height: 1;
        z-index: 2;
    }

    .colLane > div:first-child {
        width: <?php echo $hourWidth ?>px;
    }

    .colLane > div:first-child:before {
        border: 1px solid black;
    }

    .colLane > div:before {
        content: '';
        width: 0;
        height: 80vh;
        position: absolute;
        border: 1px solid rgba(0, 0, 0, 0.25);
        top: 0;
        right: 0;
    }

    .hours {
        position: sticky;
        left: 0;
        width: <?php echo $hourWidth ?>px;
        background-color: white;
        z-index: 0;
        text-align: center;
    }

    .hours > div {
        width: <?php echo $hourWidth ?>px;
        height: <?php echo $rowHeight ?>px;
        background-color: #ffc0cb52;
        border-right: 1px solid black;
    }

    .hours > div:before {
        content: "";
        position: absolute;
        left: 0;
        width: <?php echo sizeof($colnames)*$columnWidth+$hourWidth; ?>px;
        height: 0;
        border: 1px solid rgba(0, 0, 0, 0.25);
    }

    /*.table-striped tbody tr:nth-of-type(odd) {
        background-color: rgb(255, 209, 209);
    }*/
</style>
<?php
include 'inc/footer.php';