<?php
$pageTitle = "Finance";
require_once 'inc/userCheck.php';
require_once 'inc/db.php';
/** @var \PDO $db */

if (!empty($_GET)) {
    include 'inc/getFilter.php';
} else {
    $sql = 'SELECT posts.*, companies.name AS company_name, users.name AS worker_name, services.name AS service_name, u.name AS creator_name, posts.date_edited, e.name AS editor_name
                           FROM posts LEFT JOIN companies ON posts.company_id = companies.company_id LEFT JOIN users ON posts.worker_id = users.user_id
                LEFT JOIN services ON posts.service_id = services.service_id LEFT JOIN users AS u ON posts.user_id = u.user_id LEFT JOIN users AS e ON posts.person_edited = e.user_id';

    if (isset($_SESSION['company'])) {
        $sql .= " WHERE posts.company_id LIKE :companyId";
    }

    $sql .= " ORDER BY posts.date_created ASC";
    $dataQuery = $db->prepare($sql);

    if (isset($_SESSION['company'])) {
        $dataQuery->bindValue(':companyId', $_SESSION['company']);
    }

    $dataQuery->execute();
}


include 'inc/header.php';
include 'inc/navbar.php';
include 'inc/headline.php';
include 'inc/filter.php';

$posts = $dataQuery->fetchAll(PDO::FETCH_ASSOC);
$total = 0;

echo '<h2 class="py-1 px-2">Posts</h2>';

if (!empty($posts)) {
    if ($_SESSION['permission'] == 'secretary') {
        $dbCol = $db->prepare('SELECT companies.name AS company_name, users.name AS worker_name, services.name AS service_name, posts.cost, posts.payment, posts.comment,
       posts.date_created, u.name AS creator_name
                           FROM posts LEFT JOIN companies ON posts.company_id = companies.company_id LEFT JOIN users ON posts.worker_id = users.user_id
        LEFT JOIN services ON posts.service_id = services.service_id LEFT JOIN users AS u ON posts.user_id = u.user_id;');
    } else if ($_SESSION['permission'] == 'admin') {
        $dbCol = $db->prepare('SELECT companies.name AS company_name, users.name AS worker_name, services.name AS service_name, posts.cost, posts.payment, posts.comment,
       posts.date_created, u.name AS creator_name, posts.date_edited, e.name AS editor_name
                           FROM posts LEFT JOIN companies ON posts.company_id = companies.company_id LEFT JOIN users ON posts.worker_id = users.user_id
        LEFT JOIN services ON posts.service_id = services.service_id LEFT JOIN users AS u ON posts.user_id = u.user_id LEFT JOIN users AS e ON posts.person_edited = e.user_id;');
    }

    $dbCol->execute();
    $colnames = [];
    for ($i = 0; $i < $dbCol->columnCount(); $i++) {
        $colMeta = $dbCol->getColumnMeta($i);
        $colnames[] = $colMeta['name'];
    }
    echo '<div class="table-responsive-xl scrollY">
<table class="table table-striped table-bordered">
    <thead>
    <tr>
        <th scope="col">#</th>';
    foreach ($colnames as $colname) {
        echo '<th scope="col">' . htmlspecialchars($colname ?? '-') . '</th>';
    }
    echo '</tr>

    </thead>
    <tbody>';
    foreach ($posts as $index => $post) {
        echo '<tr>
        <th scope="row">' . ++$index . '.</th>
        <td>' . htmlspecialchars($post['company_name'] ?? '-') . '</td>
        <td>' . htmlspecialchars($post['worker_name'] ?? '-') . '</td>
        <td>' . htmlspecialchars($post['service_name'] ?? '-') . '</td>
        <td>' . htmlspecialchars($post['cost'] ?? '-') . '</td>
        <td>' . htmlspecialchars($post['payment'] ?? '-') . '</td>
        <td>' . htmlspecialchars($post['comment'] ?? '-') . '</td>
        <td>' . htmlspecialchars($post['date_created'] ?? '-') . '</td>
        <td>' . htmlspecialchars($post['creator_name'] ?? '-') . '</td>';
        if ($_SESSION['permission'] == 'admin') {
            echo '<td> ' . htmlspecialchars($post['date_edited'] ?? '-') . ' </td>
                <td> ' . htmlspecialchars($post['editor_name'] ?? '-') . ' </td>';
        }
        echo '</tr>';
        if (!empty($post['cost'])) {
            $total += $post['cost'];
        }
    }
    echo '</tbody>
</table>
</div>';
    ?>
    <style>
        .scrollY {
            max-height: 80vh;
            overflow-y: scroll;
        }
    </style>
    <?php
} else {
    echo '
    <div class="alert alert-info">No posts were found.</div>
    ';
}


echo '<div class="form-group">
    <label for="sum">Total: </label>
    <div class="input-group">
        <div class="input-group-prepend">
            <span class="input-group-text">€</span>
        </div>
        <input class="form-control col-7 col-sm-5 col-lg-7 col-md-7 col-xl-7" id="sum" name="sum" value="' . htmlspecialchars($total) . '">
    </div>';

echo '<label for="percent">Commission: </label>
    <div class="input-group">
        <div class="input-group-prepend">
            <span class="input-group-text">%</span>
        </div>
        <input class="form-control col-7 col-sm-5 col-lg-7 col-md-7 col-xl-7" id="percent" value="50">
        <button id="calculate">Calculate</button>
    </div>
    
    <label for="commissionTotal">Total Commission: </label>
    <div class="input-group">
        <div class="input-group-prepend">
            <span class="input-group-text">€</span>
        </div>
        <input class="form-control col-7 col-sm-5 col-lg-7 col-md-7 col-xl-7" id="commissionTotal" placeholder="' . number_format(htmlspecialchars(($total * 0.5)), 2, '.', ' ') . '" disabled>
    </div>
</div>';

echo '<script>
$(document).ready(function() {
    $("#calculate").on("click", function (){
        var percent = parseFloat($("#percent").val());
        var sum = parseFloat($("#sum").val());
        
        if (isNaN(percent)) percent = 0;
        if (isNaN(sum)) sum = 0;
        
        $("#commissionTotal").val(((sum / 100) * percent).toFixed(2));
    });
});
</script>';


//vložíme do stránek patičku
include 'inc/footer.php';
