-- phpMyAdmin SQL Dump
-- version 4.9.4
-- https://www.phpmyadmin.net/

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Databáze: `name`
--

-- --------------------------------------------------------

--
-- Table structure for table `companies`
--

CREATE TABLE IF NOT EXISTS `companies` (
  `company_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  PRIMARY KEY (`company_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_czech_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `companies`
--

INSERT INTO `companies` (`company_id`, `name`) VALUES
(1, 'Prospect Hill'),
(2, 'Mall');

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE IF NOT EXISTS `posts` (
  `post_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `company_id` int(6) unsigned DEFAULT NULL,
  `worker_id` int(11) unsigned DEFAULT NULL,
  `service_id` int(10) unsigned DEFAULT NULL,
  `cost` int(10) NOT NULL,
  `payment` enum('cash','card','voucher') NOT NULL,
  `comment` varchar(200) DEFAULT NULL,
  `date_created` datetime NOT NULL,
  `user_id` int(11) unsigned DEFAULT NULL,
  `date_edited` datetime DEFAULT NULL,
  `person_edited` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`post_id`),
  KEY `user_id` (`user_id`),
  KEY `service_id` (`service_id`),
  KEY `company_id` (`company_id`),
  KEY `worker_id` (`worker_id`),
  KEY `person_edited` (`person_edited`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_czech_ci AUTO_INCREMENT=16 ;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`post_id`, `company_id`, `worker_id`, `service_id`, `cost`, `payment`, `comment`, `date_created`, `user_id`, `date_edited`, `person_edited`) VALUES
(1, 1, 3, NULL, 50, 'cash', NULL, '2024-05-24 11:23:51', 1, NULL, NULL),
(2, 2, 3, 4, 5, 'card', NULL, '2024-05-23 11:24:24', 1, NULL, NULL),
(3, 1, 3, 2, 42, 'cash', NULL, '2024-05-22 11:24:52', 1, NULL, NULL),
(4, 1, 2, 5, 55, 'card', NULL, '2024-05-23 11:26:00', 1, NULL, NULL),
(5, 2, 3, 5, 55, 'cash', NULL, '2024-05-24 12:19:27', 1, NULL, NULL),
(6, 1, 3, 4, 50, 'cash', NULL, '2024-05-24 12:25:28', 1, NULL, NULL),
(7, 1, 3, 6, 55, 'voucher', 'voucher', '2024-05-24 12:38:07', 1, NULL, NULL),
(8, 2, 2, 3, 63, 'cash', NULL, '2024-05-23 12:57:58', 1, NULL, NULL),
(9, 1, 3, 2, 60, 'card', NULL, '2024-05-24 13:33:05', 1, NULL, NULL),
(10, 1, 3, NULL, 52, 'cash', NULL, '2024-05-24 13:57:50', 1, NULL, NULL),
(11, 2, 1, 6, 50, 'voucher', NULL, '2024-05-23 14:32:44', 1, NULL, NULL),
(12, 2, 1, 1, 52, 'card', NULL, '2024-05-24 14:33:01', 1, NULL, NULL),
(13, 2, 2, 2, 44, 'card', '20% off', '2024-05-24 14:34:45', 1, NULL, NULL),
(14, 1, 2, NULL, 87, 'card', NULL, '2024-05-22 14:55:15', 1, NULL, NULL),
(15, 1, 3, NULL, 65, 'cash', NULL, '2024-05-24 15:05:18', 1, NULL, NULL);
-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE IF NOT EXISTS `services` (
  `service_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `default_cost` smallint(6) NOT NULL,
  PRIMARY KEY (`service_id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_czech_ci AUTO_INCREMENT=7 ;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`service_id`, `name`, `default_cost`) VALUES
(1, 'Mini Manicure regular polish', 38),
(2, 'Acrylic Overlay', 50),
(3, 'Acrylic/Gel French Tips/Ombre', 63),
(4, 'Kid File & Polish', 12),
(5, 'Acrylic/Gel/BIAB FS', 55),
(6, 'BIAB base', 42);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `user_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(30) NOT NULL,
  `password` char(255) NOT NULL,
  `name` varchar(30) NOT NULL,
  `permission` enum('admin','secretary','worker') NOT NULL DEFAULT 'worker',
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_czech_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `username`, `password`, `name`, `permission`, `date_created`) VALUES
(1, 'admin', '$2y$10$SEZuHqmlqqK5608xgJ9./.uyZ43feSvAnoF1RnJJMbfuWYL8eMXEy', 'admin', 'admin', '2024-05-23 20:00:00'),
(2, 'secretary', '$2y$10$KMwcrSwG1yhiTm/cKdy59uaU31VVSqe0ONBxdsHMLVb.WSrTx4o.q', 'secretary', 'secretary', '2024-05-23 20:00:00'),
(3, 'worker', '$2y$10$lKIKBnudtb7G6ETCQKg1hOFZHZE/qEicjf7pTkXU0ha/jkAXFwOTy', 'worker', 'worker', '2024-05-23 20:00:00');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `posts`
--
ALTER TABLE `posts`
  ADD CONSTRAINT `posts_ibfk_10` FOREIGN KEY (`person_edited`) REFERENCES `users` (`user_id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `posts_ibfk_11` FOREIGN KEY (`company_id`) REFERENCES `companies` (`company_id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `posts_ibfk_12` FOREIGN KEY (`worker_id`) REFERENCES `users` (`user_id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `posts_ibfk_13` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `posts_ibfk_9` FOREIGN KEY (`service_id`) REFERENCES `services` (`service_id`) ON DELETE SET NULL ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;