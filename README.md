# Nail Business Management App
App is written in "Vanilla" PHP in "Spaghetti code", without any code structure/pattern. 

It's a web application for easier financial management of a nail business. Real time registering of services done by secretaries. The app offers financial overview, commission calculations and deletion for each employee, a scheduler which is a graphic view of registered services in a timetable format, accounts and services management, exporting/importing of data.


# Nail Business Management App detailed description(CZE)
Implementace Business Management webové aplikace v angličtině. Kód je napsaný v PHP bez návrhového vzoru. Aplikace využívá Bootstrap a jQuery.
Konkrétně je aplikace přizpůsobená pro menší nehtový podnik a je určený pro všechny zaměstnance podniku.
Real time Sekretářkou evidované provedené služby zaměstnanci(Workers). Aplikaci využívají tři různé skupiny lidí s oprávněními(admin, secretary a worker).

Databázový model - entita Posts s informacemi o zaměstnanci, typ služby(optional), ceně, typ platby, a Date s user_id info o tom kdo vytvořil příspěvek a editoval příspěvek.
Entity Users(uživatelské účty), Services, Companies, Order(jeden jediný string type atribut, který umožní uspořádat si pořadí zobrazení zaměstnanců v Scheduler a Commissions stránkách).

Funkce (role admin):
Login - Login page s radio buttons díky kterým si může uživatel při přihlášení předvolit DEFAULT Company, který uživateli automaticky vybere zvolenou Company při filtrování nebo přidávání Posts.
Dashboard - zobrazí se nejnovější vytvořené Posts. Posts jsou možné filtrovat, mazat a nebo se odkázat k editaci.
Reorder - Způsob, jak si pomocí string inputu zvolit pořadí zobrazení jednotlivých účtů v Scheduler, Commission.
Scheduler - grafické zobrazení Posts do sloupců podle příslušných zaměstnanců, je možné Posts filtrovat.
Finance - slouží k zobrazení veškerých dat zaevidovaných posts.
Commissions - zobrazí v tabulce zaměstnance podle pořadí jmen ve stránce Reorder. Tabulka ukazuje provedených služeb za vybrané období jejich total cost + 50% commission z Posts za vybrané období(defaultně se zobrazí celý daný týden od pondělí do neděle), je možné filtrovat, Posts zde lze mazat
Accounts - správa veškerých účtů, lze zde mazat, editovat a přidávat Users.
Services - správa veškerých účtů, lze zde mazat, editovat a přidávat Services.
Download - možnost importovat data do databáze z CSV, a nebo exportovat data z DB do CSV. Není ošetřeno a zabezpečeno.
Add new Post - lze zde přidávat nove Posts (tato stránka, /edit.php, bude sloužit i k editaci).

Role:
Admin - mohou vše be omezení.
Secretary - mohou pracovat s Dashboard, Add/edit Posts a Services. Dále mají přístup k Accounts s omezením.
Workers - mohou po přihlášení jen na Dashboard, kde mohou vidět pouze Posts s jejich jménem. Mohou filtrovat ale nemohou mazat ani editovat Posts.