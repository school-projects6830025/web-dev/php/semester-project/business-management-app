<?php
if ($_GET['action'] == 'add') {
    $pageTitle = 'Add new Post';
} else {
    $pageTitle = 'Edit Post';
}
require_once 'inc/userCheck.php';
require_once 'inc/db.php';
/** @var \PDO $db */

$errors = [];
$company = '';
$worker = '';
$service = '';
$cost = '';
$payment = '';
$comment = '';
$date = '';
$newDate = '';
if (!empty($_POST)) {
    if (!empty($_POST['company'])) {
        $company = $_POST['company'];
        $companyQuery = $db->prepare('SELECT * FROM companies WHERE company_id=:companyId LIMIT 1;');
        $companyQuery->execute([
            ':companyId' => $company
        ]);
        if ($companyQuery->rowCount() == 0) {
            $errors['company'] = 'Chosen company doesnt exist!';
        }
    } else {
        $errors['company'] = 'Choose company please.';
    }

    if (!empty($_POST['worker'])) {
        $worker = $_POST['worker'];
        $workerQuery = $db->prepare('SELECT * FROM users WHERE user_id=:userId LIMIT 1;');
        $workerQuery->execute([
            ':userId' => $worker
        ]);
        if ($workerQuery->rowCount() == 0) {
            $errors['worker'] = 'Chosen worker doesnt exist!';
        }
    } else {
        $errors['worker'] = 'Choose worker please.';
    }

    if (!empty($_POST['service'])) {
        $service = $_POST['service'];
        $serviceQuery = $db->prepare('SELECT * FROM services WHERE service_id=:serviceId LIMIT 1;');
        $serviceQuery->execute([
            ':serviceId' => $service
        ]);
        if ($serviceQuery->rowCount() == 0) {
            $errors['service'] = 'Chosen service doesnt exist!';
        }
    }

    if (!empty($_POST['cost']) || $_POST['cost'] == '0') {
        $cost = trim(@$_POST['cost']);
        if (!preg_match('/^-?\d{1,4}$/', $cost)) {
            $errors['cost'] = "Incorrect cost format. Must be between -9999 and 9999.";
        }
    } else {
        $errors['cost'] = 'Type in cost please.';
    }

    if (!empty($_POST['payment'])) {
        $payment = $_POST['payment'];
        $paymentQuery = $db->prepare('SELECT * FROM posts WHERE payment=:payment LIMIT 1;');
        $paymentQuery->execute([
            ':payment' => $payment
        ]);
        if ($paymentQuery->rowCount() == 0) {
            $errors['payment'] = 'Chosen payment type doesnt exist!';
        }
    } else {
        $errors['payment'] = 'Choose payment type please.';
    }

    if (!empty($_POST['comment'])) {
        $comment = trim(@$_POST['comment']);
        /*if (!preg_match("/^[a-zA-Z0-9 ]*$/",$comment) ) {
            $errors['comment'] = 'Comment must contain only letters and numbers.';
        }*/
    }

    if (empty($errors)) {
        if (isset($_GET['action'])) {
            if ($_GET['action'] == 'edit') {
                $editQuery = "UPDATE posts SET";

                if (!empty($_POST['company'])) {
                    $editQuery .= " posts.company_id=:companyId";
                }

                if (!empty($_POST['worker'])) {
                    $editQuery .= ", worker_id=:workerId";
                }

                if (!empty($_POST['service'])) {
                    $editQuery .= ", posts.service_id=:serviceId";
                }

                if (!empty($_POST['cost']) || $_POST['cost'] == 0) {
                    $editQuery .= ", cost=:cost";
                }

                if (!empty($_POST['payment'])) {
                    $editQuery .= ", payment=:payment";
                }

                if (!empty($_POST['comment'])) {
                    $editQuery .= ", comment=:comment";
                }

                $editQuery .= ", posts.date_edited=:date";

                if (!empty($_SESSION['user_id'])) {
                    $editQuery .= ", person_edited=:editor";
                }

                $editQuery .= " WHERE post_id=:postId;";

                $editSql = $db->prepare($editQuery);

                if (!empty($company)) {
                    $editSql->bindValue(':companyId', $company);
                }

                if (!empty($worker)) {
                    $editSql->bindValue(':workerId', $worker);
                }

                if (!empty($service)) {
                    $editSql->bindValue(':serviceId', $service);
                }

                if (!empty($cost) || $cost == 0) {
                    $editSql->bindValue(':cost', $cost);
                }

                if (!empty($payment)) {
                    $editSql->bindValue(':payment', $payment);
                }

                if (!empty($comment)) {
                    $editSql->bindValue(':comment', $comment);
                }

                $date = new DateTimeImmutable();
                $newDate = $date->format('Y-m-d H:i:s');
                if (!empty($newDate)) {
                    $editSql->bindValue(':date', $newDate);
                }

                if (!empty($_SESSION['user_id'])) {
                    $editSql->bindValue(':editor', $_SESSION['user_id']);
                }

                if (!empty($_GET['postId'])) {
                    $editSql->bindValue(':postId', $_GET['postId']);
                }

                $editSql->execute();

                $_SESSION['operation'] = 'success';
                $_SESSION['announce'] = 'Post edited successfully.';
                header('Location: index.php');
                exit();
            } else if ($_GET['action'] == 'add') {
                $editQuery = "INSERT INTO posts (";

                if (!empty($_POST['company'])) {
                    $editQuery .= "company_id";
                }

                if (!empty($_POST['worker'])) {
                    $editQuery .= ", worker_id";
                }

                if (!empty($_POST['service'])) {
                    $editQuery .= ", service_id";
                }

                if (!empty($_POST['cost']) || $_POST['cost'] == 0) {
                    $editQuery .= ", cost";
                }

                if (!empty($_POST['payment'])) {
                    $editQuery .= ", payment";
                }

                if (!empty($_POST['comment'])) {
                    $editQuery .= ", comment";
                }

                $editQuery .= ", posts.date_created";

                if (!empty($_SESSION['user_id'])) {
                    $editQuery .= ", user_id";
                }
                $editQuery .= ") VALUES (";

                if (!empty($_POST['company'])) {
                    $editQuery .= ":companyId";
                }

                if (!empty($_POST['worker'])) {
                    $editQuery .= ", :workerId";
                }

                if (!empty($_POST['service'])) {
                    $editQuery .= ", :serviceId";
                }

                if (!empty($_POST['cost']) || $_POST['cost'] == 0) {
                    $editQuery .= ", :cost";
                }

                if (!empty($_POST['payment'])) {
                    $editQuery .= ", :payment";
                }

                if (!empty($_POST['comment'])) {
                    $editQuery .= ", :comment";
                }

                $editQuery .= ", :date";

                if (!empty($_SESSION['user_id'])) {
                    $editQuery .= ", :userId);";
                }
                $editSql = $db->prepare($editQuery);

                if (!empty($company)) {
                    $editSql->bindValue(':companyId', $company);
                }

                if (!empty($worker)) {
                    $editSql->bindValue(':workerId', $worker);
                }

                if (!empty($service)) {
                    $editSql->bindValue(':serviceId', $service);
                }

                if (!empty($cost) || $cost == 0) {
                    $editSql->bindValue(':cost', $cost);
                }

                if (!empty($payment)) {
                    $editSql->bindValue(':payment', $payment);
                }

                if (!empty($comment)) {
                    $editSql->bindValue(':comment', $comment);
                }

                $date = new DateTimeImmutable();
                $newDate = $date->format('Y-m-d H:i:s');
                if (!empty($newDate)) {
                    $editSql->bindValue(':date', $newDate);
                }

                if (!empty($_SESSION['user_id'])) {
                    $editSql->bindValue(':userId', $_SESSION['user_id']);
                }

                $editSql->execute();

                $_SESSION['operation'] = 'success';
                $_SESSION['announce'] = 'A new Post was successfully created.';
                header('Location: edit.php?action=add');
                exit();
            }
        }
    }
}

if (isset($_GET['action'])) {
    if ($_GET['action'] == 'edit' && !empty($_GET['postId'])) {
        if (is_numeric($_GET['postId'])) {
            $query = $db->prepare('SELECT * FROM posts WHERE post_id=:postId LIMIT 1;');
            $query->execute([
                ':postId' => $_GET['postId']
            ]);
            $value = $query->fetch(PDO::FETCH_ASSOC);
            if (empty($value)) {
                $_SESSION['operation'] = 'fail';
                $_SESSION['announce'] = 'Post for editing doesn\'t exist.';
                header('Location: edit.php');
                exit();
            }
        } else {
            $_SESSION['operation'] = 'fail';
            $_SESSION['announce'] = 'Post for editing doesn\'t exist.';
            header('Location: edit.php');
            exit();
        }
    } else if ($_GET['action'] != 'add') {
        $_SESSION['operation'] = 'fail';
        $_SESSION['announce'] = 'Undefined action in Post page, returned to Dashboard.';
        header('Location: index.php');
        exit();
    }
}

include 'inc/header.php';
include 'inc/headline.php';

?>
    <form method="post">
        <?php
        echo '<a href="index.php" class="btn btn-secondary">Back</a>';
        ?>
        <div class="form-group">
            <label for="company">Company</label>
            <?php
            $query = $db->prepare('SELECT * FROM companies ORDER BY name;');
            $query->execute();
            $dbCompanies = $query->fetchAll(PDO::FETCH_ASSOC);

            if (!empty($dbCompanies)) {
                echo '<div class="btn-group btn-group-toggle" id="grp1" data-toggle="buttons">';
                if (!empty($value) && empty($company) && empty($errors['company'])) {
                    foreach ($dbCompanies as $index => $dbCompany) {
                        echo '<label class="btn btn-secondary ' . ($dbCompany['company_id'] == $value['company_id'] ? 'active' : '') . '">
                            <input type="radio" name="company" id="company' . $index . '" autocomplete="off" value="' . $dbCompany['company_id'] . '" ' . ($dbCompany['company_id'] == $value['company_id'] ? 'checked' : '') . '> ' . htmlspecialchars($dbCompany['name'] ?? '') . '
                        </label>';
                    }
                } else {
                    if (isset($_SESSION['company']) && $_SESSION['company'] != '%' && empty($_POST)) {
                        foreach ($dbCompanies as $index => $dbCompany) {
                            echo '<label class="btn btn-secondary ' . ((isset($_SESSION['company']) && $_SESSION['company'] == $dbCompany['company_id']) ? 'active' : '') . '">
                                <input type="radio" name="company" id="company' . $index . '" autocomplete="off" value="' . $dbCompany['company_id'] . '" ' . ((isset($_SESSION['company']) && $_SESSION['company'] == $dbCompany['company_id']) ? 'checked' : '') . '> ' . htmlspecialchars($dbCompany['name'] ?? '') . '
                            </label>';
                        }
                    } else {
                        foreach ($dbCompanies as $index => $dbCompany) {
                            echo '<label class="btn btn-secondary ' . ((isset($_POST['company']) && $_POST['company'] == $dbCompany['company_id']) ? 'active' : '') . '">
                            <input type="radio" name="company" id="company' . $index . '" autocomplete="off" value="' . $dbCompany['company_id'] . '" ' . ((isset($_POST['company']) && $_POST['company'] == $dbCompany['company_id']) ? 'checked' : '') . '> ' . htmlspecialchars($dbCompany['name'] ?? '') . '
                        </label>';
                        }
                    }
                }
            } else {
                echo 'Code BUG: no $dbUsers';
            }
            ?>
        </div>
        <?php
        if (!empty($errors['company'])) {
            echo '<div class="invalid-feedback" style="display: flex">' . $errors['company'] . '</div>';
        }
        ?>
        </div>

        <div class="form-group">
            <label for="worker">Employee name</label>
            <?php
            $query = $db->prepare('SELECT * FROM users ORDER BY name;');
            $query->execute();
            $dbUsers = $query->fetchAll(PDO::FETCH_ASSOC);
            ?>
            <select name="worker" id="worker"
                    class="form-control <?php echo(!empty($errors['worker']) ? 'is-invalid' : ''); ?>">
                <option value="">--Choose--</option>
                <?php
                if (!empty($dbUsers)) {
                    if (!empty($value) && empty($worker) && empty($errors['worker'])) {
                        foreach ($dbUsers as $user) {
                            echo '<option value="' . $user['user_id'] . '" ' . ($user['user_id'] == $value['worker_id'] ? 'selected' : '') . '>' . htmlspecialchars($user['name'] ?? '') . '</option>';
                        }
                    } else {
                        foreach ($dbUsers as $user) {
                            echo '<option value="' . $user['user_id'] . '" ' . ((isset($_POST['worker']) && $_POST['worker'] == $user['user_id']) ? 'selected' : '') . '>' . htmlspecialchars($user['name'] ?? '') . '</option>';
                        }
                    }
                } else {
                    echo 'Code BUG: no $dbUsers';
                }
                ?>
            </select>
            <?php
            if (!empty($errors['worker'])) {
                echo '<div class="invalid-feedback" style="display: flex">' . $errors['worker'] . '</div>';
            }
            ?>
        </div>

        <div class="form-group">
            <div class="form-row">
                <label for="service">Service Name</label>
                <div class="text-muted font-italic">&nbsp;(Optional)</div>
            </div>
            <?php
            $query = $db->prepare('SELECT * FROM services ORDER BY name;');
            $query->execute();
            $dbServices = $query->fetchAll(PDO::FETCH_ASSOC);
            ?>
            <select name="service" id="service"
                    class="form-control <?php echo(!empty($errors['service']) ? 'is-invalid' : ''); ?>">
                <option value="">--Choose--</option>
                <?php
                if (!empty($dbServices)) {
                    if (!empty($value) && empty($service) && empty($errors['service'])) {
                        foreach ($dbServices as $dbService) {
                            echo '<option value="' . $dbService['service_id'] . '" data-price="' . $dbService['default_cost'] . '" ' . ($dbService['service_id'] == $value['service_id'] ? 'selected' : '') . '>' . htmlspecialchars($dbService['name'] ?? '') . '</option>';
                        }
                    } else {
                        foreach ($dbServices as $dbService) {
                            echo '<option value="' . $dbService['service_id'] . '" data-price="' . $dbService['default_cost'] . '" ' . ((isset($_POST['service']) && $_POST['service'] == $dbService['service_id']) ? 'selected' : '') . '>' . htmlspecialchars($dbService['name'] ?? '') . '</option>';
                        }
                    }
                } else {
                    echo 'Code BUG: no dbservices';
                }
                ?>
            </select>
            <?php
            if (!empty($errors['service'])) {
                echo '<div class="invalid-feedback" style="display: flex">' . $errors['service'] . '</div>';
            }
            ?>
        </div>

        <div class="form-group" id="costValidation">
            <label for="cost" id="costLabel">Cost</label>
            <?php
            if (!empty($value) && empty($cost) && empty($errors['cost'])) {
                ?>
                <input type="number" min="-9999" max="9999" class="form-control" id="cost" name="cost"
                       value="<?php echo htmlspecialchars($value['cost']) ?>">
                <?php
            } else {
                ?>
                <input type="number" min="-9999" max="9999" class="form-control <?php echo (!empty($errors['cost']) ? 'is-invalid' : ''); ?>" id="cost" name="cost"
                       value="<?php echo htmlspecialchars($cost) ?>">
            <?php
            }
            echo '<div class="invalid-feedback">Cost must be between -9999 and 9999</div>';

            ?>
        </div>
        <script>
            $(document).ready(function () {
                $("#service").change(function () {
                    let price = $(this).find("option:selected").data("price");
                    $("#cost").val(price);
                });

                $("#cost").on("input", function() {
                    if (/^-?\d{1,4}$/.test($(this).val()) || !$(this).val()) {
                        $("#costValidation").removeClass('was-validated')
                    } else {
                        $("#costValidation").addClass('was-validated')
                    }
                });
            });
        </script>

        <div class="form-group">
            <label for="payment">Payment</label>
            <?php
            echo '<div class="btn-group btn-group-toggle" id="grp2" data-toggle="buttons">';
            if (!empty($value) && empty($payment) && empty($errors['payment'])) {
                echo '<label class="btn btn-secondary ' . (($value['payment'] == "cash") ? 'active' : '') . '">
                            <input type="radio" name="payment" id="payment0" autocomplete="off" value="cash" ' . (($value['payment'] == "cash") ? 'checked' : '') . '>cash</label>';
                echo '<label class="btn btn-secondary ' . (($value['payment'] == "card") ? 'active' : '') . '">
                            <input type="radio" name="payment" id="payment1" autocomplete="off" value="card" ' . (($value['payment'] == "card") ? 'checked' : '') . '>card</label>';
                echo '<label class="btn btn-secondary ' . (($value['payment'] == "voucher") ? 'active' : '') . '">
                            <input type="radio" name="payment" id="payment2" autocomplete="off" value="voucher" ' . (($value['payment'] == "voucher") ? 'checked' : '') . '>voucher</label>';

            } else {
                echo '<label class="btn btn-secondary ' . ((isset($_POST['payment']) && $_POST['payment'] == "cash") ? 'active' : '') . '">
                            <input type="radio" name="payment" id="payment0" autocomplete="off" value="cash" ' . ((isset($_POST['payment']) && $_POST['payment'] == "cash") ? 'checked' : '') . '>cash</label>';
                echo '<label class="btn btn-secondary ' . ((isset($_POST['payment']) && $_POST['payment'] == "card") ? 'active' : '') . '">
                            <input type="radio" name="payment" id="payment1" autocomplete="off" value="card" ' . ((isset($_POST['payment']) && $_POST['payment'] == "card") ? 'checked' : '') . '>card</label>';
                echo '<label class="btn btn-secondary ' . ((isset($_POST['payment']) && $_POST['payment'] == "voucher") ? 'active' : '') . '">
                            <input type="radio" name="payment" id="payment2" autocomplete="off" value="voucher" ' . ((isset($_POST['payment']) && $_POST['payment'] == "voucher") ? 'checked' : '') . '>voucher</label>';
            }
            ?>
        </div>
        <?php
        if (!empty($errors['payment'])) {
            echo '<div class="invalid-feedback" style="display: flex">' . $errors['payment'] . '</div>';
        }
        ?>
        </div>
        <script>
            $(document).ready(function () {
                $(".btn-group-toggle label").click(function () {
                    $(this).siblings().removeClass('active');
                    $(this).addClass('active');
                    $(this).parent().find('input[type="radio"]').prop('checked', false);
                    $(this).find('input[type="radio"]').prop('checked', true);
                });
            });
        </script>

        <div class="form-group">
            <div class="form-row">
                <label for="comment">Comment</label>
                <div class="text-muted font-italic">&nbsp;(Optional)</div>
            </div>
            <?php
            if (!empty($value) && empty($comment) && empty($errors['comment'])) {
                echo '<textarea class="form-control" maxlength="200" name="comment" id="comment" >' . htmlspecialchars($value['comment'] ?? '') . '</textarea>';
            } else {
                echo '<textarea class="form-control ' . (!empty($errors['comment']) ? 'is-invalid' : '') . '" maxlength="200" name="comment" id="comment" >' . htmlspecialchars($comment) . '</textarea>';
            }
            ?>
        </div>
        <?php
        /*if (!empty($errors['comment'])) {
            echo '<div class="invalid-feedback" style="display: flex">' . $errors['comment'] . '</div>';
        }*/
        ?>
        <button type="submit" class="btn btn-primary" name="process">Submit</button>
    </form>

<?php

include 'inc/footer.php';
