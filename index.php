<?php
$pageTitle = "Dashboard";
require_once 'inc/userCheck.php';
require_once 'inc/db.php';
/** @var \PDO $db */

if (!empty($_POST) && $_SESSION['permission']!='worker') {
    if (isset($_POST['delete']) && isset($_POST['confirmSingleFlag'])) {
        if ($_POST['confirmSingleFlag'] === 'true') {
            $deleteQuery = $db->prepare("DELETE FROM posts WHERE post_id=:postId;");
            $deleteQuery->execute([
                ':postId' => $_POST['delete']
            ]);
            $_SESSION['operation'] = 'success';
            $_SESSION['announce'] = 'Post deleted.';
        }

    } else if (isset($_POST['deleteChecked'])) {
        if (isset($_POST['ids']) && isset($_POST['confirmFlag'])) {
            if ($_POST['confirmFlag'] === 'true') {
                foreach ($_POST['ids'] as $el) {
                    $deleteQuery = $db->prepare("DELETE FROM posts WHERE post_id=:postId;");
                    $deleteQuery->execute([
                        ':postId' => $el
                    ]);
                }
                $_SESSION['operation'] = 'success';
                $_SESSION['announce'] = 'Checked posts deleted.';
            }
        } else {
            $_SESSION['operation'] = 'fail';
            $_SESSION['announce'] = 'No posts checked, nothing to delete.';
        }
    }
}
if (!empty($_GET)) {
    include 'inc/getFilter.php';
} else {
    if ($_SESSION['permission'] == 'admin' || $_SESSION['permission'] == 'secretary') {
        if (isset($_SESSION['company'])) {
            $dataQuery = $db->prepare('SELECT posts.*, companies.name AS company_name, users.name AS worker_name, services.name AS service_name, u.name AS creator_name
                           FROM posts LEFT JOIN companies ON posts.company_id = companies.company_id LEFT JOIN users ON posts.worker_id = users.user_id
                            LEFT JOIN services ON posts.service_id = services.service_id LEFT JOIN users AS u ON posts.user_id = u.user_id WHERE posts.company_id LIKE :companyId ORDER BY date_created DESC;');
            $dataQuery->bindValue(':companyId', $_SESSION['company']);
        } else {
            $dataQuery = $db->prepare('SELECT
                           posts.*, companies.name AS company_name, users.name AS worker_name, services.name AS service_name, u.name AS creator_name
                           FROM posts LEFT JOIN companies ON posts.company_id = companies.company_id LEFT JOIN users ON posts.worker_id = users.user_id
                            LEFT JOIN services ON posts.service_id = services.service_id LEFT JOIN users AS u ON posts.user_id = u.user_id ORDER BY date_created DESC;');
        }
        $dataQuery->execute();
    } else {
        $dataQuery = $db->prepare('SELECT
                           posts.*, companies.name AS company_name, users.name AS worker_name, services.name AS service_name, u.name AS creator_name
                           FROM posts LEFT JOIN companies ON posts.company_id = companies.company_id LEFT JOIN users ON posts.worker_id = users.user_id
        LEFT JOIN services ON posts.service_id = services.service_id LEFT JOIN users AS u ON posts.user_id = u.user_id WHERE posts.company_id LIKE :companyId AND posts.worker_id=:workerId ORDER BY date_created DESC;');
        $dataQuery->execute([
            ':companyId' => $_SESSION['company'],
            'workerId' => $_SESSION['user_id']
        ]);
    }
}

include 'inc/header.php';
include 'inc/navbar.php';
include 'inc/headline.php';
include 'inc/filter.php';

$posts = $dataQuery->fetchAll(PDO::FETCH_ASSOC);

echo '<h2 class="py-1 px-2">Posts</h2>';

if ($_SESSION['permission'] == 'worker' && !empty($posts)) {
    $total = 0;
    foreach ($posts as $post) {
        $total += $post['cost'];
    }
    echo '<div class="form-row no-gutters">
        <div class="form-group col-6 col-sm-4 col-md-3 col-lg-2 col-xl-2" style="min-width:130px; max-width: 150px">
            <div class="input-group">';
    echo '<label for="total">Total: </label>';
    echo '
    <div class="input-group" id="total" style="max-width: 150px; min-width: 150px">
        <div class="input-group-prepend">
            <span class="input-group-text">€</span>
        </div>
        <input class="form-control" id="sum" name="sum" value="' . htmlspecialchars($total) . '" disabled>
    </div>
    </div>
    </div>
    
    <div class="form-group col-6 col-sm-4 col-md-3 col-lg-2 col-xl-2" style="min-width:140px; max-width: 150px">
            <div class="input-group">
    <label for="totalCom">Total Commission: </label>
    <div class="input-group" id="totalCom" style="max-width: 150px; min-width: 150px">
        <div class="input-group-prepend">
            <span class="input-group-text">€</span>
        </div>
        <input class="form-control" id="commissionTotal" placeholder="' . (htmlspecialchars(($total * 0.5))) . '" disabled>
    </div>
    </div>
    </div>
    
</div>';
}

if ($_SESSION['permission'] != 'worker') {
    include 'inc/deleteChecked.php';
}

if (!empty($posts)) {
    echo '<div class="row no-gutters justify-content-center scrollY">';
    foreach ($posts as $index => $post) {
        echo '<article class="col col-sm-6 col-md-5 col-lg-4 col-xl-3 border border-dark mx-1 my-1 px-2 py-1">';
        echo '<div class="d-flex justify-content-between">
                <div>
                    <span class="font-weight-bold">' . ($index + 1) . '.</span>
                    <span class="badge badge-secondary">' . htmlspecialchars($post['company_name']) . '</span>
                    <span class="badge badge-warning">' . htmlspecialchars($post['worker_name']) . '</span>
                    </div>';

                if ($_SESSION['permission'] != 'worker') echo '<input type="checkbox" class="checkbox" name="ids[]" value="' . $post['post_id'] . '">';
               echo '</div>';

        echo '<div class="badge badge-info mb-1">' . htmlspecialchars($post['service_name'] ?? 'none') . '</div>';

        echo '<div class="row no-gutters justify-content-between">
                <div class="col-6 col-sm-6 col-md-6 col-lg-6 col-xl-6">
                <div class="d-flex">
                    <div class="small text-muted">';
        echo '<div>' . date('d.m.Y H:i', strtotime($post['date_created'])) . " " . $post['creator_name'] . '</div>';
        echo '<div class="border border-dark rounded" style="box-sizing: content-box">';
        if (!empty($post['comment'])) {
            echo '* ';
            echo htmlspecialchars($post['comment']);
        }
        echo '</div>';
        echo '</div> 
                </div> 
                </div>';

        echo '<div class="form-group col-4 col-sm-4 col-md-4 col-lg-4 col-xl-4" style="min-width:90px; max-width:120px">
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text">€</span>
                        </div>
                        <span class="form-control">' . htmlspecialchars($post['cost']) . '</span>
                    </div>';

        if ($post['payment'] == 'voucher') {
            echo '<div class="badge align-self-center" style="background-color:pink">' . htmlspecialchars($post['payment']) . '</div>';
        } else if ($post['payment'] == 'card') {
            echo '<div class="badge badge-dark align-self-center">' . htmlspecialchars($post['payment']) . '</div>';
        } else if ($post['payment'] == 'cash') {
            echo '<div class="badge badge-primary align-self-center">' . htmlspecialchars($post['payment']) . '</div>';
        }

        echo '</div> </div>';

        if ($_SESSION['permission'] != 'worker') {
            echo '<div class="row no-gutters justify-content-between align-bottom">
                    <a href="edit.php?action=edit&postId=' . $post['post_id'] . '" class="btn btn-light">Edit post</a>
                    <button type="submit" class="btn btn-danger" onclick="return confirmSingleDelete()" name="delete" id="delete'.$index.'" value="' . $post['post_id'] . '">Delete</button>
                  </div>';
        }
        echo '</article>';
    }
    echo '</div>';
    echo '<input type="hidden" name="confirmSingleFlag" id="confirmSingleFlag" value="">';
    echo '</form>';
} else {
    echo '<div class="alert alert-info">No posts were found.</div>';
}
?>
    <script>
        function confirmSingleDelete() {
            if (confirm("Delete Single Post?")) {
                document.getElementById('confirmSingleFlag').value = 'true';
            } else {
                this.preventDefault()
            }
        }

        $(function () {
            $('.check_all').click(function () {
                $('.checkbox').prop('checked', true);
            });
            $('.uncheck_all').click(function () {
                $('.checkbox').prop('checked', false);
            });
        });
    </script>

    <style>
        .scrollY {
            max-height: 80vh;
            overflow-y: scroll;
        }
    </style>

<?php
include 'inc/footer.php';
