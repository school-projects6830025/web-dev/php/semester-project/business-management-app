<?php
session_start();
$pageTitle = "Login";
require_once 'inc/db.php';
/** @var \PDO $db */

$errors = [];
if (!empty($_POST)) {
    if (empty($_POST['username']) || empty($_POST['password'])) {
        $errors['login'] = 'Type both username and password please!';
    }

    if (empty($errors['login'])) {
        $query = $db->prepare('SELECT * FROM users WHERE username=:username LIMIT 1;');
        $query->execute([
            ':username' => $_POST['username']
        ]);
        $rows = $query->fetchAll(PDO::FETCH_ASSOC);
        if (!empty($rows)) {
            $userDb = $rows['0'];
            if (password_verify($_POST['password'], $userDb['password'])) {
                //prihlaseni
                $_SESSION['user_id'] = $userDb['user_id'];
                $_SESSION['name'] = $userDb['name'];
                $_SESSION['permission'] = $userDb['permission'];
                if (isset($_POST['company'])) {
                    $_SESSION['company'] = $_POST['company'];
                }
                if ($_SESSION['permission'] == 'worker') {
                    header('Location: index.php');
                } else if ($_SESSION['permission'] == 'secretary') {
                    header('Location: index.php');
                } else if ($_SESSION['permission'] == 'admin') {
                    header('Location: index.php');
                }
            } else {
                $errors['login'] = 'Wrong username or password!';
            }
        } else {
            $errors['login'] = 'Wrong username or password!';
        }
    }
}

include 'inc/header.php';
?>

    <form method="post">
        <div class="form-group col-md-8 col-lg-6 col-xl-5 rounded border border-secondary p-4">
            <h2 class="py-1"><?php echo $pageTitle ?></h2>
            <label for="username" class="font-weight-bold">Username:</label><br/>
            <input type="text" class="form-control <?php echo(!empty($errors['login']) ? 'is-invalid' : '') ?>"
                   name="username" id="username" value="<?php echo htmlspecialchars($_POST['username'] ?? '') ?>"/>
            <?php
            if (!empty($errors['login'])) {
                echo '<div class="invalid-feedback" style="display: flex">' . $errors['login'] . '</div>';
            }
            ?>
            <label for="password" class="font-weight-bold">Password:</label><br/>
            <input type="password" class="form-control <?php echo(!empty($errors['login']) ? 'is-invalid' : '') ?>"
                   name="password" id="password">
            <?php
            if (!empty($errors['login'])) {
                echo '<div class="invalid-feedback" style="display: flex">' . $errors['login'] . '</div>';
            }
            ?>
            <div class="row no-gutters justify-content-between">
                <button type="submit" class="btn btn-primary my-2">Login</button>
                <div class="form-group">
                    <label>Company:</label>
                    <?php
                    $query = $db->prepare('SELECT * FROM companies ORDER BY name;');
                    $query->execute();
                    $dbCompanies = $query->fetchAll(PDO::FETCH_ASSOC);

                    if (!empty($dbCompanies)) {
                        $index = 0;
                        echo '<div class="btn-group btn-group-toggle my-2" data-toggle="buttons">';
                        echo '<label class="btn btn-secondary ' . (((isset($_POST['company']) && $_POST['company'] == "%") || empty($_POST)) ? 'active' : '') . '">
                            <input type="radio" name="company" id="company'.$index++.'" autocomplete="off" value="%" ' . (((isset($_POST['company']) && $_POST['company'] == "%") || empty($_POST)) ? 'checked' : '') . '>All
                        </label>';
                        foreach ($dbCompanies as $dbCompany) {
                            echo '<label class="btn btn-secondary ' . ((isset($_POST['company']) && $_POST['company'] == $dbCompany['company_id']) ? 'active' : '') . '">
                                <input type="radio" name="company" id="company'.$index++.'" autocomplete="off" value="' . $dbCompany['company_id'] . '" ' . ((isset($_POST['company']) && $_POST['company'] == $dbCompany['company_id']) ? 'checked' : '') . '> ' . htmlspecialchars($dbCompany['name'] ?? '') . '
                            </label>';
                        }
                    }
                    ?>
                </div>
            </div>
        </div>
    </form>
    <script>
        $(document).ready(function(){
            $(".btn-group-toggle label").click(function() {
                // Remove the 'active' class from all labels in the same group
                $(this).siblings().removeClass('active');

                // Add the 'active' class to the clicked label
                $(this).addClass('active');

                // Uncheck all radio buttons in the same group
                $(this).parent().find('input[type="radio"]').prop('checked', false);

                // Check the clicked radio button
                $(this).find('input[type="radio"]').prop('checked', true);
            });
        });
    </script>

<?php
include 'inc/footer.php';