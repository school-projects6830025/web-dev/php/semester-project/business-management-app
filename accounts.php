<?php
$pageTitle = 'Accounts';
require_once 'inc/userCheck.php';
require_once 'inc/db.php';
/** @var \PDO $db */

$errors = [];
$newPassword = '';
$date = '';
$irishDate = '';
$newDate = '';
$username = '';
$password = '';
$name = '';
$permission = '';
if (!empty($_POST)) {
    if (isset($_POST['delete']) && isset($_POST['confirmSingleFlag'])) {
        if ($_POST['confirmSingleFlag'] === 'true') {
            if ($_POST['delete'] == $_SESSION['user_id']) {
                $_SESSION['operation'] = 'fail';
                $_SESSION['announce'] = 'Cannot delete own account.';
                header('Location: accounts.php');
                exit();
            }
            $slctQuery = $db->prepare("SELECT * FROM users WHERE users.user_id=:userId;");
            $slctQuery->execute([
                ':userId' => $_POST['delete']
            ]);
            $deleteUser = $slctQuery->fetch(PDO::FETCH_ASSOC);
            if ($deleteUser['permission'] == 'admin' && $_SESSION['permission'] == 'secretary') {
                $_SESSION['operation'] = 'fail';
                $_SESSION['announce'] = 'You don\'t have permission to delete admin account.';
            } else {
                $deleteQuery = $db->prepare("DELETE FROM users WHERE users.user_id=:userId;");
                $deleteQuery->execute([
                    ':userId' => $_POST['delete']
                ]);
                $selectQuery = $db->prepare("SELECT * FROM orders LIMIT 1;");
                $selectQuery->execute();
                $oldOrder = $selectQuery->fetch(PDO::FETCH_ASSOC);

                $newOrder = explode(',', $oldOrder['order']);
                $key = array_search(strtolower($deleteUser['name']), $newOrder);
                if ($key !== false) {
                    unset($newOrder[$key]);
                    $newOrder = implode(',', $newOrder);

                    $updateQuery = $db->prepare("UPDATE orders SET orders.order=:order WHERE order_id='1';");
                    $updateQuery->execute([
                        ':order' => $newOrder
                    ]);
                }
            }
            if (!isset($_SESSION['operation']) && !isset($_SESSION['announce'])) {
                $_SESSION['operation'] = 'success';
                $_SESSION['announce'] = 'Account deleted.';
            }
        }

    } else if (isset($_POST['deleteChecked'])) {
        if (isset($_POST['ids']) && isset($_POST['confirmFlag'])) {
            if ($_POST['confirmFlag'] === 'true') {
                if (in_array($_SESSION['user_id'], $_POST['ids'])) {
                    $_SESSION['operation'] = 'fail';
                    $_SESSION['announce'] = 'Cannot delete own account.';
                } else {
                    $selectQuery = $db->prepare("SELECT * FROM orders LIMIT 1;");
                    $selectQuery->execute();
                    $oldOrder = $selectQuery->fetch(PDO::FETCH_ASSOC);
                    $newOrder = explode(',', $oldOrder['order']);
                    foreach ($_POST['ids'] as $el) {
                        $userQuery = $db->prepare("SELECT * FROM users WHERE users.user_id=:userId;");
                        $userQuery->execute([
                            ':userId' => $el
                        ]);
                        $user = $userQuery->fetch(PDO::FETCH_ASSOC);
                        $deleteQuery = $db->prepare("DELETE FROM users WHERE users.user_id=:userId;");
                        $deleteQuery->execute([
                            ':userId' => $el
                        ]);
                        $key = array_search(strtolower($user['name']), $newOrder);
                        if ($key !== false) {
                            unset($newOrder[$key]);
                        }
                    }
                    $newOrder = implode(',', $newOrder);
                    $updateQuery = $db->prepare("UPDATE orders SET orders.order=:order WHERE order_id='1';");
                    $updateQuery->execute([
                        ':order' => $newOrder
                    ]);

                    $_SESSION['operation'] = 'success';
                    $_SESSION['announce'] = 'Checked Accounts deleted.';
                }
            }
        } else {
            $_SESSION['operation'] = 'fail';
            $_SESSION['announce'] = 'No Accounts checked, nothing to delete.';
        }

    } else {
        if (isset($_GET['action']) && $_GET['action'] == 'edit' && !empty($_GET['userId'])) {
            if (is_numeric($_GET['userId'])) {
                $userQuery = $db->prepare('SELECT * FROM users WHERE users.user_id=:userId LIMIT 1;');
                $userQuery->execute([
                    ':userId' => $_GET['userId']
                ]);
                if ($userQuery->rowCount() == 0) {
                    $_SESSION['operation'] = 'fail';
                    $_SESSION['announce'] = 'This Account doesnt exist!';
                    header('Location: accounts.php');
                    exit();
                }
                $dbUser = $userQuery->fetch(PDO::FETCH_ASSOC);
                if ($dbUser['permission'] == 'admin' && $_SESSION['permission'] != 'admin') {
                    $_SESSION['operation'] = 'fail';
                    $_SESSION['announce'] = 'You cannot edit this Account! Refreshing Accounts page.';
                    header('Location: accounts.php');
                    exit();
                }

            } else {
                $_SESSION['operation'] = 'fail';
                $_SESSION['announce'] = 'This Account doesn\'t exist, try again!';
                header('Location: accounts.php');
                exit();
            }
        }

        if (!empty($_POST['username'])) {
            $username = trim(@$_POST['username']);
            if (!preg_match("/^[a-zA-Z]{4,}$/",$_POST['username']) ) {
                $errors['username'] = 'Login username needs to be at least 4 letters long please.';
            }
        }

        if (!empty($_POST['password'])) {
            if (mb_strlen($_POST['password'], 'utf-8') < 6 || !preg_match('/[0-9]/', $_POST['password']) || !preg_match('/[A-Z]/', $_POST['password'])) {
                $errors['password'] = 'Password must be atleast 6 characters long, contains atleast one number and one uppercase letter.';
            } else if ( preg_match('/^\s/', $_POST['password']) || preg_match('/\s$/', $_POST['password']) ) {
                $errors['password'] = 'Password can\'t begin or end with a white space.';
            } else if (empty($_POST['password2'])) {
                $errors['password'] = " ";
                $errors['password2'] = 'Please re-enter your password to confirm.';
            } else if ($_POST['password2'] != $_POST['password']) {
                $errors['password'] = " ";
                $errors['password2'] = 'Confirm Password does not match with Password.';
            } else {
                $password = $_POST['password'];
            }

        } else {
            if (!isset($_GET['action'])) {
                $errors['password'] = 'Password must be atleast 6 characters long, contains atleast one number and one uppercase letter.';
                $errors['password2'] = 'Please enter passwords.';
            }
        }

        if (!empty($_POST['name'])) {
            $name = trim(@$_POST['name']);
            if (!preg_match("/^[a-zA-Z ]{2,}$/",$_POST['name']) ) {
                $errors['name'] = 'Employee name must be at least 2 letters long please.';
            }
            if (!isset($_GET['action'])) {
                $nameQuery = $db->prepare('SELECT users.name FROM users;');
                $nameQuery->execute();
                $dbUsers = $nameQuery->fetchAll(PDO::FETCH_ASSOC);
                if (!empty($dbUsers)) {
                    $query = $db->prepare('SELECT * FROM orders;');
                    $query->execute();
                    $dbOrder = $query->fetch(PDO::FETCH_ASSOC);
                    $spaces = str_replace(' ', '', $dbOrder['order']);
                    $orderArr = explode(',', $spaces);
                    if (in_array(strtolower($name), $orderArr)) {
                        $errors['name'] = 'Account name is too similar to an existing account. Please select a different name for the account (case insensitive).';
                    }
                }
                /*
                $nameQuery = $db->prepare('SELECT * FROM users WHERE users.name=:name LIMIT 1;');
                $nameQuery->execute([
                    ':name' => $name
                ]);
                if ($nameQuery->rowCount() != 0) {
                    $errors['name'] = 'This account name already exists!';
                }
                */
            }
        } else {
            $errors['name'] = 'Type in employee name please.';
        }

        if (!empty($_POST['permission'])) {
            $permission = $_POST['permission'];
            $permissionQuery = $db->prepare('SELECT * FROM users WHERE permission=:permission LIMIT 1;');
            $permissionQuery->execute([
                ':permission' => $permission
            ]);
            if ($permissionQuery->rowCount() == 0) {
                $errors['permission'] = 'Chosen permission doesnt exist!';
            }
        } else {
            $errors['permission'] = 'Choose account permission please.';
        }

        if (empty($errors)) {
            if (isset($_GET['action'])) {
                if ($_GET['action'] == 'edit' && isset($_GET['userId'])) {
                    $slctQuery = $db->prepare("SELECT * FROM users WHERE users.user_id=:userId;");
                    $slctQuery->execute([
                        ':userId' => $_GET['userId']
                    ]);
                    $user = $slctQuery->fetch(PDO::FETCH_ASSOC);

                    $editQuery = "UPDATE users SET";

                    if (!empty($_POST['username'])) {
                        $editQuery .= " username=:username";
                    }

                    if (!empty($_POST['password'])) {
                        $editQuery .= ", password=:password";
                    }

                    if (!empty($_POST['name'])) {
                        $editQuery .= ", users.name=:name";
                    }

                    if (!empty($_POST['permission'])) {
                        $editQuery .= ", permission=:permission";
                    }

                    $editQuery .= " WHERE users.user_id=:userId;";
                    $editSql = $db->prepare($editQuery);

                    if (!empty($username)) {
                        $editSql->bindValue(':username', $username);
                    }

                    if (!empty($password)) {
                        $newPassword = password_hash("$password", PASSWORD_DEFAULT);
                        $editSql->bindValue(':password', $newPassword);
                    }

                    if (!empty($name)) {
                        $editSql->bindValue(':name', $name);
                    }

                    if (!empty($permission)) {
                        $editSql->bindValue(':permission', $permission);
                    }

                    if (!empty($_GET['userId'])) {
                        $editSql->bindValue(':userId', $_GET['userId']);
                    }

                    $editSql->execute();

                    $_SESSION['operation'] = 'success';
                    $_SESSION['announce'] = 'Account ' . $user["name"] . ' edited successfully.';

                    $selectQuery = $db->prepare("SELECT * FROM orders LIMIT 1;");
                    $selectQuery->execute();
                    $oldOrder = $selectQuery->fetch(PDO::FETCH_ASSOC);
                    $newOrder = str_replace($user['name'], strtolower($name), $oldOrder['order']);
                    $updateQuery = $db->prepare("UPDATE orders SET orders.order=:order WHERE order_id='1';");
                    $updateQuery->execute([
                        ':order' => $newOrder
                    ]);

                    header('Location: accounts.php');
                    exit();

                } else {
                    $_SESSION['operation'] = 'fail';
                    $_SESSION['announce'] = 'Undefined action, Accounts page refreshed.';
                    header('Location: accounts.php');
                    exit();
                }
            } else {
                $editQuery = "INSERT INTO users (";

                if (!empty($_POST['username'])) {
                    $editQuery .= "username";
                }

                if (!empty($_POST['password'])) {
                    $editQuery .= ", password";
                }

                if (!empty($_POST['name'])) {
                    $editQuery .= ", users.name";
                }

                if (!empty($_POST['permission'])) {
                    $editQuery .= ", permission";
                }

                $editQuery .= ", users.date_created";
                $editQuery .= ") VALUES (";

                if (!empty($_POST['username'])) {
                    $editQuery .= ":username";
                }

                if (!empty($_POST['password'])) {
                    $editQuery .= ", :password";
                }

                if (!empty($_POST['name'])) {
                    $editQuery .= ", :name";
                }

                if (!empty($_POST['permission'])) {
                    $editQuery .= ", :permission";
                }

                $editQuery .= ", :date);";
                $editSql = $db->prepare($editQuery);

                if (!empty($username)) {
                    $editSql->bindValue(':username', $username);
                }

                if (!empty($password)) {
                    $newPassword = password_hash("$password", PASSWORD_DEFAULT);
                    $editSql->bindValue(':password', $newPassword);
                }

                if (!empty($name)) {
                    $editSql->bindValue(':name', $name);
                }

                if (!empty($permission)) {
                    $editSql->bindValue(':permission', $permission);
                }

                $date = new DateTimeImmutable();
                $newDate = $date->format('Y-m-d H:i:s');
                if (!empty($newDate)) {
                    $editSql->bindValue(':date', $newDate);
                }

                $editSql->execute();

                $_SESSION['operation'] = 'success';
                $_SESSION['announce'] = 'A new Account was successfully created.';

                $selectQuery = $db->prepare("SELECT * FROM orders LIMIT 1;");
                $selectQuery->execute();
                $oldOrder = $selectQuery->fetch(PDO::FETCH_ASSOC);
                $oldOrder['order'] .= "," . strtolower($name);
                $updateQuery = $db->prepare("UPDATE orders SET orders.order=:order;");
                $updateQuery->execute([
                    ':order' => $oldOrder['order']
                ]);

                header('Location: accounts.php');
                exit();
            }
        }
    }
}

if (isset($_GET['action'])) {
    if ($_GET['action'] == 'edit' && !empty($_GET['userId'])) {
        if (is_numeric($_GET['userId'])) {
            $query = $db->prepare('SELECT * FROM users WHERE user_id=:userId LIMIT 1;');
            $query->execute([
                ':userId' => $_GET['userId']
            ]);
            $value = $query->fetch(PDO::FETCH_ASSOC);
            if (empty($value)) {
                $_SESSION['operation'] = 'fail';
                $_SESSION['announce'] = 'Account for editing doesn\'t exist.';
                header('Location: accounts.php');
                exit();
            }
        } else {
            $_SESSION['operation'] = 'fail';
            $_SESSION['announce'] = 'Account for editing doesn\'t exist.';
            header('Location: accounts.php');
            exit();
        }
    } else {
        $_SESSION['operation'] = 'fail';
        $_SESSION['announce'] = 'Undefined action, Accounts page refreshed.';
        header('Location: accounts.php');
        exit();
    }
}

include 'inc/header.php';
include 'inc/headline.php';
?>
    <form method="post">
        <div class="row no-gutters">
            <?php
            echo '<a href="index.php" class="btn btn-secondary">Back</a>';
            if (!empty($value)) {
                echo '<span class="font-weight-bold" style="align-self: center;">Currently editing - ' . htmlspecialchars($value['name']) . '</span>';
            }
            ?>
        </div>

        <div class="form-group">
            <label for="username">Username</label>
            <?php
            if (!empty($value) && empty($username) && empty($errors['username'])) {
                echo '<input type="text" maxlength="30" class="form-control" id="username" name="username" value="' . htmlspecialchars($value['username'] ?? '') . '" autocomplete="off">';
            } else {
                echo '<input type="text" maxlength="30" class="form-control ' . (!empty($errors['username']) ? 'is-invalid' : '') . '" id="username" name="username" value="' . htmlspecialchars($username ?? '') . '" placeholder="Login username" autocomplete="off">';
            }
            if (!empty($errors['username'])) {
                echo '<div class="invalid-feedback" style="display: flex">' . $errors['username'] . '</div>';
            }
            ?>
        </div>

        <div class="form-group">
            <label for="password">Password</label>
            <?php
            if (!empty($value) && empty($password) && empty($errors['password'])) {
                echo '<input type="password" maxlength="40" class="form-control" id="password" name="password" value="">';
            } else {
                echo '<input type="password" maxlength="40" class="form-control ' . (!empty($errors['password']) ? 'is-invalid' : '') . '" id="password" name="password" value="" placeholder="Password" autocomplete="off">';
            }
            if (!empty($errors['password'])) {
                echo '<div class="invalid-feedback" style="display: flex">' . $errors['password'] . '</div>';
            }
            ?>
        </div>

        <div class="form-group">
            <label for="password2">Confirm Password</label>
            <?php
            if (!empty($value) && empty($password2) && empty($errors['password2'])) {
                echo '<input type="password" maxlength="40" class="form-control" id="password2" name="password2" value="">';
            } else {
                echo '<input type="password" maxlength="40" class="form-control ' . (!empty($errors['password2']) ? 'is-invalid' : '') . '" id="password2" name="password2" value="" placeholder="Confirm Password" autocomplete="off">';
            }
            if (!empty($errors['password2'])) {
                echo '<div class="invalid-feedback" style="display: flex">' . $errors['password2'] . '</div>';
            }
            ?>
        </div>

        <div class="form-group">
            <label for="name">Name</label>
            <?php
            if (!empty($value) && empty($name) && empty($errors['name'])) {
                echo '<input type="text" maxlength="30" class="form-control" id="name" name="name" value="' . htmlspecialchars($value['name'] ?? '') . '" autocomplete="off">';
            } else {
                echo '<input type="text" maxlength="30" class="form-control ' . (!empty($errors['name']) ? 'is-invalid' : '') . '" id="name" name="name" value="' . htmlspecialchars($name ?? '') . '" placeholder="Employee Full name(max 30 characters long)" autocomplete="off">';
            }
            if (!empty($errors['name'])) {
                echo '<div class="invalid-feedback" style="display: flex">' . $errors['name'] . '</div>';
            }
            ?>
        </div>

        <div class="form-group">
            <label for="permission">Permission</label>
            <?php
            $query = $db->prepare('SELECT DISTINCT permission FROM users;');
            $query->execute();
            $dbUsers = $query->fetchAll(PDO::FETCH_ASSOC);
            ?>
            <select name="permission" id="permission"
                    class="form-control <?php echo(!empty($errors['permission']) ? 'is-invalid' : ''); ?>">
                <option value="">--Choose--</option>
                <?php
                if (!empty($dbUsers)) {
                    if (!empty($value) && empty($permission) && empty($errors['permission'])) {
                        foreach ($dbUsers as $user) {
                            if ($user['permission'] != 'admin') {
                                echo '<option value="' . $user['permission'] . '" ' . ($user['permission'] == $value['permission'] ? 'selected' : '') . '>' . htmlspecialchars($user['permission']) . '</option>';
                            } else if ($_SESSION['permission'] == 'admin') {
                                echo '<option value="' . $user['permission'] . '" ' . ($user['permission'] == $value['permission'] ? 'selected' : '') . '>' . htmlspecialchars($user['permission']) . '</option>';
                            }
                        }
                    } else {
                        foreach ($dbUsers as $user) {
                            if ($user['permission'] != 'admin') {
                                echo '<option value="' . $user['permission'] . '" ' . ((isset($_POST['permission']) && $_POST['permission'] == $user['permission']) ? 'selected' : '') . '>' . htmlspecialchars($user['permission']) . '</option>';
                            } else if ($_SESSION['permission'] == 'admin') {
                                echo '<option value="' . $user['permission'] . '" ' . ((isset($_POST['permission']) && $_POST['permission'] == $user['permission']) ? 'selected' : '') . '>' . htmlspecialchars($user['permission']) . '</option>';
                            }
                        }
                    }
                } else {
                    echo 'Code BUG: no $dbUsers';
                }
                ?>
            </select>
            <?php
            if (!empty($errors['permission'])) {
                echo '<div class="invalid-feedback" style="display: flex">' . $errors['permission'] . '</div>';
            }
            ?>
        </div>
        <button type="submit" class="btn btn-primary" name="process">Submit</button>
    </form>

<?php
$query = $db->prepare('SELECT * FROM users ORDER BY date_created DESC;');
$query->execute();
$accounts = $query->fetchAll(PDO::FETCH_ASSOC);

echo '<h2 class="py-1 px-2">Accounts</h2>';
include 'inc/deleteChecked.php';

if (!empty($accounts)) {
    echo '<div class="row no-gutters scrollY">';
    foreach ($accounts as $index => $account) {
        if ($_SESSION['permission'] != 'admin' && $account['permission'] == 'admin') {
            continue;
        }
        echo '<article class="col col-sm-6 col-md-5 col-lg-4 col-xl-3 border border-dark mx-1 my-1 px-2 py-1">';
        echo '<div class="d-flex justify-content-between">
            <div>
                <span class="font-weight-bold">' . ($index + 1) . '.</span>
                <span class="badge badge-warning">' . htmlspecialchars($account['name']) . '</span>
            </div>
            
            <input type="checkbox" class="checkbox" name="ids[]" value="' . $account['user_id'] . '">
        </div>';

        echo '<div class="badge badge-dark mb-1">' . htmlspecialchars($account['permission'] ?? '-') . '</div>';

        echo '<div class="d-flex">
                    <div class="small text-muted">';
        echo ' ';
        echo '<div>' . date('d.m.Y H:i:s', strtotime($account['date_created'])) . '</div>';
        echo ' ';
        echo '</div>
            </div>';

        echo '<div class="row no-gutters justify-content-between align-bottom">
                        <a href="accounts.php?action=edit&userId=' . $account['user_id'] . '" class="btn btn-light" style="position:relative;left:4%">Edit account</a>
                        <button type="submit" class="btn btn-danger" onclick="return confirmSingleDelete()" name="delete" id="delete'.$index.'" value="' . $account['user_id'] . '">Delete</button>
                    </div>';
        echo '</article>';
    }
    echo '</div>';
    echo '<input type="hidden" name="confirmSingleFlag" id="confirmSingleFlag" value="">';
    echo '</form>';
} else {
    echo '<div class="alert alert-info">No accounts were found.</div>';
}

?>
    <script>
        function confirmSingleDelete() {
            if (confirm("Delete Single Account?")) {
                document.getElementById('confirmSingleFlag').value = 'true';
            } else {
                document.getElementById('confirmSingleFlag').value = 'false';
            }
        }

        $(function () {
            $('.check_all').click(function () {
                $('.checkbox').prop('checked', true);
            });
            $('.uncheck_all').click(function () {
                $('.checkbox').prop('checked', false);
            });
        });
    </script>

    <style>
        .scrollY {
            max-height: 80vh;
            overflow-y: scroll;
        }
    </style>
<?php

include 'inc/footer.php';
